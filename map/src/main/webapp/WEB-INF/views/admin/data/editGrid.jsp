<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>企业报表--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/layui/css/grid.css" media="all" />
</head>
<body class="childrenBody">
<form id="gridEditForm" class="layui-form">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">省份</label>
		<input id="id" name="id" type="hidden"  value="${grid.id}">
		<input id="entType" name="entType" type="hidden"  value="${grid.entType}">
		<div class="layui-input-block">
			<c:choose>
				<c:when test="${grid.entType==1}">
					<input type="text" id="province" disabled name="province" value="${grid.province}" class="layui-input layui-disabled" lay-verify="required" placeholder="请输入省份名称">
				</c:when>
				<c:when test="${grid.entType==0}">
					<input type="text" id="province" disabled name="province" value="黑龙江" class="layui-input layui-disabled" lay-verify="required" placeholder="请输入省份名称">
				</c:when>
			</c:choose>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">地市</label>
		<div class="layui-input-block">
			<c:choose>
				<c:when test="${grid.entType==1}">
					<input type="text" id="cityName" disabled name="cityName" value="${grid.cityName}" class="layui-input layui-disabled" lay-verify="required" placeholder="请输入地市名称">
				</c:when>
				<c:when test="${grid.entType==0}">
					<input type="text" id="cityName" disabled name="cityName" value="${grid.city}" class="layui-input layui-disabled" lay-verify="required" placeholder="请输入地市名称">
				</c:when>
			</c:choose>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">公司名称</label>
		<div class="layui-input-block">
			<c:choose>
				<c:when test="${grid.entType==1}">
					<input type="text" id="companyName" name="companyName" value="${grid.companyName}" class="layui-input companyName" lay-verify="required" placeholder="请输入公司名称">
				</c:when>
				<c:when test="${grid.entType==0}">
					<input type="text" id="companyName" name="companyName" value="${grid.name}" class="layui-input companyName" lay-verify="required" placeholder="请输入公司名称">
				</c:when>
			</c:choose>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">行业</label>
		<div class="layui-input-block">
			<select id="industry" name="industry" class="industry" lay-filter="userGrade">
			<option value="">请选择行业</option>
	        <option value="公共管理、社会保障和社会组织">公共管理、社会保障和社会组织</option>
	        <option value="电力、热力燃气及水产和供应业">电力、热力燃气及水产和供应业</option>
	        <option value="水利、环境和公共设施管理业">水利、环境和公共设施管理业</option>
	        <option value="采矿业">采矿业</option>
	        <option value="建筑业">建筑业</option>
	        <option value="批发和零售业">批发和零售业</option>
	        <option value="交通运输、仓储和邮政业">交通运输、仓储和邮政业</option>
	        <option value="农、林、牧、渔业">农、林、牧、渔业</option>
	        <option value="制造业">制造业</option>
	        <option value="租赁和商务服务业">租赁和商务服务业</option>
	        <option value="科学研究和技术服务业">科学研究和技术服务业</option>
	        <option value="居民服务、修理和其他服务业">居民服务、修理和其他服务业</option>
	        <option value="信息传输、软件和信息技术服务业">信息传输、软件和信息技术服务业</option>
	        <option value="金融业">金融业</option>
	        <option value="房地产业">房地产业</option>
	        <option value="卫生和社会工作">卫生和社会工作</option>
	        <option value="教育">教育</option>
	        <option value="文化、体育和娱乐业">文化、体育和娱乐业</option>
			</select>
		</div>
	</div>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">状态</label>
				<div class="layui-input-block">
					<input type="text" id="state" name="state" value="${grid.state}" class="layui-input state" lay-verify="required" placeholder="请输入状态">
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">法人</label>
				<div class="layui-input-block">
					<input type="text" id="legal" name="legal" value="${grid.legal}" class="layui-input legal" lay-verify="required" placeholder="请输入法人">
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">注册资本</label>
				<div class="layui-input-block">
					<input type="text" id="registered" name="registered" value="${grid.registered}" class="layui-input registered" lay-verify="required" placeholder="请输入注册资本">
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">联系电话</label>
				<div class="layui-input-block">
					<input type="text" id="telephone" name="telephone" value="${grid.telephone}" class="layui-input telephone" lay-verify="required" placeholder="请输入联系电话">
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">企业地址</label>
		<div class="layui-input-block">
			<input type="text" id="address" name="address" value="${grid.address}" class="layui-input address" lay-verify="required" placeholder="请输入地址">
		</div>
	</div>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">企业网址</label>
				<div class="layui-input-block">
					<input type="text" id="website" name="website" value="${grid.website}" class="layui-input website" placeholder="请输入网址">
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">企业邮箱</label>
				<div class="layui-input-block">
					<input type="text" id="email" name="email" value="${grid.email}" class="layui-input email" lay-verify="required" placeholder="请输入邮箱">
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">发展状态</label>
		<div class="layui-input-block">
			<select id="develop" name="develop" class="develop" lay-filter="develop">
			<option value="0">请选择</option>
	        <option value="1">已发展</option>
	        <option value="2">未发展</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">企业类型</label>
		<div class="layui-input-block">
			<select id="type" name="type" class="type" lay-filter="type">
			<option value="">请选择</option>
	        <option value="党政军">党政军</option>
	        <option value="大企业">大企业</option>
	        <option value="金融">金融</option>
	        <option value="商客">商客</option>
	        <option value="高校">高校</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">企业规模</label>
		<div class="layui-input-block">
			<select id="scale" name="scale" class="scale" lay-filter="scale">
			<option value="">请选择</option>
	        <option value="大型企业">大型企业</option>
	        <option value="中型企业">中型企业</option>
	        <option value="小型企业">小型企业</option>
	        <option value="微型企业">微型企业</option>
			</select>
		</div>
	</div>
	<c:choose>
		<c:when test="${grid.entType==1}">
			<div class="layui-form-item layui-row layui-col-xs12">
				<label class="layui-form-label">经营范围</label>
				<div class="layui-input-block">
					<textarea id="scope" name="scope" class="layui-textarea scope" lay-verify="required" placeholder="请输入经营范围"></textarea>
				</div>
			</div>
		</c:when>
		<c:when test="${grid.entType==0}">
		</c:when>
	</c:choose>
	<div class="layui-form-item layui-row layui-col-xs12 product_right">
		<div class="layui-upload-list">
			<img id="linkLogoImg" src="${grid.picture}" class="layui-upload-img layui-circle linkLogoImg">
		</div>
	</div>
	 <!-- 
	<div class="layui-form-item">
		<div class="layui-upload">
			<img class="layui-upload-img layui-circle productFaceBtn productAvatar" src="${product.picture}" id="linkLogoImg">
		</div>
		<button id="linkLogoBtn" type="button" class="layui-btn layui-btn-primary productFaceBtn"><i class="layui-icon">&#xe67c;</i>产品图片</button>
	</div>
	-->
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="editGrid">确认修改</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">取消</button>
		</div>
	</div>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/data/gridEdit.js"></script>
</body>
</html>