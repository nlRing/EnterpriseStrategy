<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>分享产品--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">产品名称</label>
		<input id="id" name="id" type="hidden"  value="${product.id}">
		<div class="layui-input-block">
			<input type="text" id="name" name="name" value="${product.name}" class="layui-input name" lay-verify="required" readonly="readonly">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12 product_right">
		<div class="layui-upload-list">
			<img class="layui-upload-img layui-circle productFaceBtn productAvatar" src="${product.picture}" id="productFace">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">产品资费</label>
		<div class="layui-input-block">
			<input type="text" id="postage" name="postage" value="${product.postage}" class="layui-input postage" lay-verify="required" placeholder="请输入产品资费" readonly="readonly">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">产品介绍</label>
		<div class="layui-input-block">
			<textarea id="introduce" name="introduce" required lay-verify="required" class="layui-textarea" readonly="readonly">
				${product.introduce}
			</textarea>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">开通时效</label>
		<div class="layui-input-block">
			<select id="prescription" name="prescription" class="layui-input prescription" lay-filter="layui-input prescription" disabled="disabled">
				<option value="0">立即开通</option>
				<option value="1">下月开通</option>
				<option value="2">下季度开通</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button id="fx" class="layui-btn layui-btn-sm layui-btn-normal" lay-submit="" lay-filter="">QQ分享</button>
			<button id="wx" class="layui-btn layui-btn-sm" lay-submit="" lay-filter="">微信分享</button>
			<button id="dx" class="layui-btn layui-btn-sm layui-btn-primary" lay-submit="" lay-filter="">短信分享</button>
			<button id="dy" class="layui-btn layui-btn-sm layui-btn-warm" onclick="printit();">打印</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	function printit(){
		if(confirm('确认打印吗？')){
			document.getElementById('fx').style.display="none";//隐藏
			document.getElementById('wx').style.display="none";//隐藏
			document.getElementById('dx').style.display="none";//隐藏
			document.getElementById('dy').style.display="none";//隐藏
			window.print();//打印
			document.getElementById('fx').style.display="inline";//显示
			document.getElementById('wx').style.display="inline";//显示
			document.getElementById('dx').style.display="inline";//显示
			document.getElementById('dy').style.display="inline";//显示
		}
	}
</script>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/js/cacheUserInfo.js"></script>
<script type="text/javascript" src="${path }/static/page/product/productEdit.js"></script>
</body>
</html>