<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>添加产品--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">产品名称</label>
		<div class="layui-input-block">
			<input type="text" id="name" name="name" class="layui-input name" lay-verify="required" placeholder="请输入产品名称">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12 product_right">
		<div class="layui-upload-list">
			<img class="layui-upload-img layui-circle productFaceBtn productAvatar" id="productFace">
		</div>
		<button type="button" class="layui-btn layui-btn-primary productFaceBtn"><i class="layui-icon">&#xe67c;</i>产品图片</button>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">产品资费</label>
		<div class="layui-input-block">
			<input type="text" id="postage" name="postage" class="layui-input postage" lay-verify="required" placeholder="请输入产品资费">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">产品介绍</label>
		<div class="layui-input-block">
			<textarea id="introduce" name="introduce" required lay-verify="required" placeholder="请输入产品介绍" class="layui-textarea"></textarea>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">开通时效</label>
		<div class="layui-input-block">
			<select id="prescription" name="prescription" class="layui-input prescription" lay-filter="layui-input prescription">
				<option value="0">立即开通</option>
				<option value="1">下月开通</option>
				<option value="2">下季度开通</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="addProduct">立即添加</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">取消</button>
		</div>
	</div>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/product/productAdd.js"></script>
<script type="text/javascript" src="${path }/static/js/cacheUserInfo.js"></script>
</body>
</html>