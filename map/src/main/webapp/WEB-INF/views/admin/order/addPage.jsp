<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>添加用户--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
   <blockquote class="layui-elem-quote quoteBox">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">工单名称</label>
		<div class="layui-input-block">
			<input type="text" id="name" name="name" class="layui-input name" lay-verify="required" placeholder="请输入登工单名称">
		</div>
	</div>
	<div class="layui-form-item layui-form-text">
		<label class="layui-form-label">工单描述</label>
		<div class="layui-input-block">
			<textarea id="describe" name="describe" class="layui-textarea" lay-verify="required" placeholder="请输入" ></textarea>
		</div>
	</div>
    <div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">人员</label>
		<div class="layui-input-inline layui-form" style="width:436px">
			<input type="text" id="employeeId" name="employeeId" class="layui-input" lay-verify="required" placeholder="请选择人员">
			<input type="hidden" id="userId" name="userId" class="layui-input">
		</div>
	</div>
	<div class="layui-inline">
		  <a class="layui-btn addTask_btn">选择任务</a>
	</div>
	</blockquote>
	<table id="taskList" lay-filter="taskList"></table>
	<!--操作-->
	<script type="text/html" id="taskListBar">
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
	</script>
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button id="uploadFile" class="layui-btn layui-btn-sm" lay-submit="" lay-filter="createOrder">立即添加</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">取消</button>
		</div>
	</div>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/order/addPage.js"></script>
</body>
</html>