<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>工单管理--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form">
	<blockquote class="layui-elem-quote quoteBox">
		<div class="layui-inline">
			<label class="layui-form-label">时间范围:</label>
			<div class="layui-inline">
				<input type="text" class="layui-input" id="release" placeholder="请选择日期范围" readonly />
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">工单名称:</label>
			<div class="layui-inline">
				<input type="text" class="layui-input" id="name" placeholder="请输入工单名称" />
			</div>
		</div>
		<shiro:hasRole name="admin">
		<div class="layui-inline">
			<label class="layui-form-label">录入工号:</label>
			<div class="layui-inline">
				<input type="text" class="layui-input" id="userId" placeholder="请输入工号" />
			</div>
		</div>
		</shiro:hasRole>
		<div class="layui-inline">
			<label class="layui-form-label">地区选择:</label>
			<div class="layui-input-inline layui-form">
				<select id="city" name="city" lay-filter="city" disabled>
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="area" name="area" lay-filter="area" disabled>
					<option value="">请选择县/区</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<a class="layui-btn search_btn" data-type="reload">确定</a>
		</div>
		<shiro:hasAnyRoles name="admin,pm">
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal createOrder_btn">创建工单</a>
		</div>
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal addOrder_btn">明细工单导入</a>
		</div>
		</shiro:hasAnyRoles>
	</blockquote>
	<table id="orderList" lay-filter="orderList"></table>
	<script type="text/html" id="orderListBar">
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="details">订单详情</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="allocate">分配</a>
	</script>
</form>
  <input type="hidden" id="orderId" name="orderId" class="layui-input">
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/order/orderList.js"></script>
</body>
</html>