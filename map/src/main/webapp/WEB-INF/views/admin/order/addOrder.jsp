<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>添加用户--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">工单名称</label>
		<div class="layui-input-block">
			<input type="text" id="name" name="name" class="layui-input name" lay-verify="required" placeholder="请输入登工单名称">
		</div>
	</div>
	<div class="layui-form-item layui-form-text">
		<label class="layui-form-label">工单描述</label>
		<div class="layui-input-block">
			<textarea id="describe" name="describe" class="layui-textarea" lay-verify="required" placeholder="请输入" ></textarea>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">地市</label>
		<div class="layui-input-inline layui-form" style="width:436px">
			<select id="city" name="city" lay-filter="city" lay-verify="required" disabled>
				<option value="">请选择市</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">区县</label>
		<div class="layui-input-inline layui-form" style="width:436px">
			<select id="area" name="area" lay-filter="area" lay-verify="required" disabled>
				<option value="">请选择县/区</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item">
		<div class="layui-inline">
			<label class="layui-form-label">上传文件</label>
			<div class="layui-input-inline" style="width: 300px">
				<input type="text" id="file" name="file" class="layui-input layui-disabled" lay-verify="required" placeholder="请上传文件" disabled>
			</div>
		</div>
		<button type="button" class="layui-btn" id="uploadBtn"><i class="layui-icon">&#xe67c;</i>上传文件</button>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">主推产品</label>
		<div class="layui-input-block">
			<select id="Pproduct" name="Pproduct" class="userGrade" lay-filter="Pproduct" >
				<option value="">请选择</option>
			</select>
		</div>
	</div>
	<!-- 
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">热推产品</label>
		<div class="layui-input-block">
			<select id="Hotproduct" name="Hotproduct" class="userGrade" lay-filter="Hotproduct" >
				<option value="">请选择</option>
			</select>
		</div>
	</div>
	 -->
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button id="uploadFile" class="layui-btn layui-btn-sm" lay-submit="" lay-filter="addOrder">立即添加</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">取消</button>
		</div>
	</div>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/order/orderAdd.js"></script>
</body>
</html>