<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>添加任务</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
  <legend>企业列表</legend>
</fieldset>
<form class="layui-form" style="width:100%;">
   	<blockquote class="layui-elem-quote quoteBox">
			<div class="layui-inline">
				<div class="layui-input-inline">
					<input type="text" class="layui-input searchVal" placeholder="请输入搜索的内容" />
				</div>
				<a class="layui-btn search_btn" data-type="reload">搜索</a>
			</div>
			<div class="layui-inline">
			<label class="layui-form-label">地区:</label>
			<div class="layui-input-inline layui-form">
				<select id="province" name="province" lay-filter="province" class="province">
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="city" name="city" lay-filter="city" disabled>
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="area" name="area" lay-filter="area" disabled>
					<option value="">请选择县/区</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">行业选择:</label>
			<div class="layui-input-inline">
				<select id="industry" name="industry" lay-filter="industry">
					<option value="">请选择行业</option>
			        <option value="公共管理、社会保障和社会组织">公共管理、社会保障和社会组织</option>
			        <option value="电力、热力燃气及水产和供应业">电力、热力燃气及水产和供应业</option>
			        <option value="水利、环境和公共设施管理业">水利、环境和公共设施管理业</option>
			        <option value="采矿业">采矿业</option>
			        <option value="建筑业">建筑业</option>
			        <option value="批发和零售业">批发和零售业</option>
			        <option value="交通运输、仓储和邮政业">交通运输、仓储和邮政业</option>
			        <option value="农、林、牧、渔业">农、林、牧、渔业</option>
			        <option value="制造业">制造业</option>
			        <option value="租赁和商务服务业">租赁和商务服务业</option>
			        <option value="科学研究和技术服务业">科学研究和技术服务业</option>
			        <option value="居民服务、修理和其他服务业">居民服务、修理和其他服务业</option>
			        <option value="信息传输、软件和信息技术服务业">信息传输、软件和信息技术服务业</option>
			        <option value="金融业">金融业</option>
			        <option value="房地产业">房地产业</option>
			        <option value="卫生和社会工作">卫生和社会工作</option>
			        <option value="教育">教育</option>
			        <option value="文化、体育和娱乐业">文化、体育和娱乐业</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">发展状态:</label>
			<div class="layui-input-inline">
				<select id="develop" name="develop" lay-filter="develop">
					<option value="0">全部</option>
			        <option value="1">已发展</option>
			        <option value="2">未发展</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">企业类型:</label>
			<div class="layui-input-inline">
				<select id="type" name="type" lay-filter="type">
					<option value="0">全部</option>
			        <option value="1">党政军</option>
			        <option value="2">大企业</option>
			        <option value="3">金融</option>
			        <option value="4">商客</option>
			        <option value="5">高校</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">企业规模:</label>
			<div class="layui-input-inline">
				<select id="scale" name="scale" lay-filter="scale">
					<option value="0">全部</option>
			        <option value="1">大型企业</option>
			        <option value="2">中型企业</option>
			        <option value="3">小型企业</option>
			        <option value="4">微型企业</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="radio" name="way"  value="hm" title="手动分配" lay-filter="choosemode" checked="">
                <input type="radio" name="way"  value="at" title="自动分配" lay-filter="choosemode">
			</div>
		</div>
	</blockquote>
	<table id="gridList" lay-filter="gridList"></table>
	
	<span style="display:inline-block;width:100%;text-align:center;color:#999999;margin-top: 22px;font-size: 14px; ">
                        <label id="lableNum" style="margin-right: 20px;">已勾选0个企业</label>
    </span>	
	<div id = 'usertable'>
	 <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
      <legend>成员列表</legend>
     </fieldset>
	 <table id="userList" lay-filter="userList"></table>
	 </div>
     <hr class="layui-bg-green">
    
	<div class="layui-form-item layui-row layui-col-xs12">
	<div id = 'gridtable'></div>	<div class="layui-input-block">
		<p>
			<button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="addbatch">确认分配</button>
		</p>
		</div>
	</div>
		
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/allocatedlist/allocatedlistAdd.js"></script>
</body>
</html>