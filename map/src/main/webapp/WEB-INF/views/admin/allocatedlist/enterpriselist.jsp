<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>企业报表--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/layui/css/grid.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form">
	<blockquote class="layui-elem-quote quoteBox">
		<form class="layui-form">
			<div class="layui-inline">
				<div class="layui-input-inline">
					<input type="text" class="layui-input searchVal" placeholder="请输入搜索的内容" />
				</div>
				<a class="layui-btn search_btn" data-type="reload">搜索</a>
			</div>
			<div class="layui-inline">
			<!-- 
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">城市选择:</label>
			<div class="layui-input-inline">
				<select id="city" name="city" lay-filter="city">
					<option value="">请选择城市</option>
			        <option value="哈尔滨">哈尔滨</option>
			        <option value="齐齐哈尔">齐齐哈尔</option>
			        <option value="鸡西">鸡西</option>
			        <option value="鹤岗">鹤岗</option>
			        <option value="双鸭山">双鸭山</option>
			        <option value="大庆">大庆</option>
			        <option value="宜春">宜春</option>
			        <option value="佳木斯">佳木斯</option>
			        <option value="七台河">七台河</option>
			        <option value="牡丹江">牡丹江</option>
			        <option value="黑河">黑河</option>
			        <option value="绥化">绥化</option>
			        <option value="大兴安岭地区">大兴安岭地区</option>
				</select>
			</div>
			 -->
			<label class="layui-form-label">地区:</label>
			<div class="layui-input-inline layui-form">
				<select id="province" name="province" lay-filter="province" class="province">
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="city" name="city" lay-filter="city" disabled>
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="area" name="area" lay-filter="area" disabled>
					<option value="">请选择县/区</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">行业选择:</label>
			<div class="layui-input-inline">
				<select id="industry" name="industry" lay-filter="industry">
					<option value="">请选择行业</option>
			        <option value="公共管理、社会保障和社会组织">公共管理、社会保障和社会组织</option>
			        <option value="电力、热力燃气及水产和供应业">电力、热力燃气及水产和供应业</option>
			        <option value="水利、环境和公共设施管理业">水利、环境和公共设施管理业</option>
			        <option value="采矿业">采矿业</option>
			        <option value="建筑业">建筑业</option>
			        <option value="批发和零售业">批发和零售业</option>
			        <option value="交通运输、仓储和邮政业">交通运输、仓储和邮政业</option>
			        <option value="农、林、牧、渔业">农、林、牧、渔业</option>
			        <option value="制造业">制造业</option>
			        <option value="租赁和商务服务业">租赁和商务服务业</option>
			        <option value="科学研究和技术服务业">科学研究和技术服务业</option>
			        <option value="居民服务、修理和其他服务业">居民服务、修理和其他服务业</option>
			        <option value="信息传输、软件和信息技术服务业">信息传输、软件和信息技术服务业</option>
			        <option value="金融业">金融业</option>
			        <option value="房地产业">房地产业</option>
			        <option value="卫生和社会工作">卫生和社会工作</option>
			        <option value="教育">教育</option>
			        <option value="文化、体育和娱乐业">文化、体育和娱乐业</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">发展状态:</label>
			<div class="layui-input-inline">
				<select id="develop" name="develop" lay-filter="develop">
					<option value="0">全部</option>
			        <option value="1">已发展</option>
			        <option value="2">未发展</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">企业类型:</label>
			<div class="layui-input-inline">
				<select id="type" name="type" lay-filter="type">
					<option value="0">全部</option>
			        <option value="1">党政军</option>
			        <option value="2">大企业</option>
			        <option value="3">金融</option>
			        <option value="4">商客</option>
			        <option value="5">高校</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">企业规模:</label>
			<div class="layui-input-inline">
				<select id="scale" name="scale" lay-filter="scale">
					<option value="0">全部</option>
			        <option value="1">大型企业</option>
			        <option value="2">中型企业</option>
			        <option value="3">小型企业</option>
			        <option value="4">微型企业</option>
				</select>
			</div>
		</div>
		</form>
	</blockquote>
	<table id="gridList" lay-filter="gridList"></table>
	<!--操作-->
	<script type="text/html" id="gridListBar">
		<a class="layui-btn layui-btn-xs" lay-event="choose">添加</a>
	</script>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/task/enterpriseList.js"></script>
</body>
</html>