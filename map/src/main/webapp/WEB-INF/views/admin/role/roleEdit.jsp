<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>文章列表--layui后台管理模板 2.0</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form id="roleEditForm" class="layui-form" style="width:80%;">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">名称</label>
		<input id="id" name="id" type="hidden"  value="${role.id}">
		<div class="layui-input-block">
			<input type="text" id="name" name="name" value="${role.name}" class="layui-input userName" lay-verify="required" placeholder="请输入登录名">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">描述</label>
		<div class="layui-input-block">
			<input type="text" id="description" name="description" value="${role.description}" class="layui-input userName" lay-verify="required" placeholder="请输入描述">
		</div>
	</div>
	
			<input type="hidden" id="seq" name="seq" value="${role.seq}" class="layui-input userName" lay-verify="required">

	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">用户状态</label>
		<div class="layui-input-block">
			<select id="status" name="status" class="status" lay-filter="userGrade">
				<option value="0">正常</option>
				<option value="1">停用</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="editRole">提交</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">重置</button>
		</div>
	</div>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/role/roleEdit.js"></script>
<script type="text/javascript">
$("#status").find("option[value = '"+${role.status}+"']").attr("selected","selected")
</script>
</body>
</html>