<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>添加任务</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
<form class="layui-form" style="width:100%;">
  <blockquote class="layui-elem-quote quoteBox">
    <div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">工单名称</label>
		<div class="layui-input-block">
			<input type="text" id="name" name="name" class="layui-input name" lay-verify="required" placeholder="请输入登工单名称">
		</div>
	</div>
	<div class="layui-form-item layui-form-text">
		<label class="layui-form-label">工单描述</label>
		<div class="layui-input-block">
			<textarea id="describe" name="describe" class="layui-textarea" lay-verify="required" placeholder="请输入" ></textarea>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">地市</label>
		<div class="layui-input-inline layui-form" style="width:436px">
			<select id="city" name="city" lay-filter="city" lay-verify="required" disabled>
				<option value="">请选择市</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">区县</label>
		<div class="layui-input-inline layui-form" style="width:436px">
			<select id="area" name="area" lay-filter="area" lay-verify="required" disabled>
				<option value="">请选择县/区</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">主推产品</label>
		<div class="layui-input-block">
			<input type="text" id="product1" name="product1" class="layui-input" lay-verify="required" placeholder="请选择产品">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">热推产品</label>
		<div class="layui-input-block">
			<input type="text" id="product2" name="product2" class="layui-input" lay-verify="required" placeholder="请选择产品">
		</div>
	</div>
	<div class="layui-inline">
		  <a class="layui-btn addEnterprise_btn">选择企业</a>
	</div>
		<input type="hidden" id="primaryP" name="primaryP" class="layui-input"  lay-verify="">
		<input type="hidden" id="hotP" name="hotP" class="layui-input"  lay-verify="">	
   </blockquote>
	<table id="gridList" lay-filter="gridList"></table>
	<!--操作-->
	<script type="text/html" id="gridListBar">
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
	</script>
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="addTask">立即添加</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary resettable">重置</button>
		</div>
	</div>
			
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/task/taskAdd.js"></script>
</body>
</html>