<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>任务管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="childrenBody">
	<blockquote class="layui-elem-quote quoteBox">
		<form class="layui-form">
		  <div class="layui-inline">
				<div class="layui-input-inline">
					<input id="companyName" type="text" class="layui-input" placeholder="请输入企业名称" />
				</div>
				<div class="layui-input-inline">
					<select id="type" lay-verify="" class="layui-input">
					  <option value="">请选择类型</option>
					  <option value="0">未分配</option>
					  <option value="1">已分配</option>
					  <option value="2">导入</option>
					</select>     
				</div>
			</div>
			<a class="layui-btn search_btn" data-type="reload">搜索</a>
			<div class="layui-inline">
				<a class="layui-btn layui-btn-normal addNews_btn">添加任务</a>
			</div>
		</form>
	</blockquote>
	<table id="taskList" lay-filter="taskList"></table>
    
	<!--操作-->
	<script type="text/html" id="taskListBar">
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
	</script>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/task/taskList.js"></script>
</body>
</html>