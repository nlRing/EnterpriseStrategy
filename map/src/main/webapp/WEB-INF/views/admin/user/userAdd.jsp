<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>添加用户--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
	<script type="text/javascript">
    //$(function() {
    //	$('#roleIds').combotree({
    //        url : '${path }/role/tree',
    //        multiple: true,
    //        required: true,
    //        panelHeight : 'auto'
    //    });
    //});
    </script>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">登录名</label>
		<div class="layui-input-block">
			<input type="text" id="loginName" name="loginName" class="layui-input userName" lay-verify="required" placeholder="请输入登录名">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">姓名</label>
		<div class="layui-input-block">
			<input type="text" id="name" name="name" class="layui-input userName" lay-verify="required" placeholder="请输入姓名">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">密码</label>
		<div class="layui-input-block">
			<input type="password" id="password" name="password" class="layui-input userName" lay-verify="required|password" placeholder="请输入密码">
			<!-- 
			<i id="passwordeye" class="layui-icon layui-icon-search"></i>
			 -->
		</div>
	</div>
	<div class="layui-row">
		<div class="magb15 layui-col-md4 layui-col-xs12">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block">
				<select id="sex" name="sex" class="userGrade" lay-filter="userGrade">
					<option value="0" selected="selected">男</option>
					<option value="1">女</option>
				</select>
			</div>
		</div>
		<div class="magb15 layui-col-md4 layui-col-xs12">
			<label class="layui-form-label">用户类型</label>
			<div class="layui-input-block">
				<select id="userType" name="userType" class="userGrade" lay-filter="userGrade">
					<option value="0">管理员</option>
					<option value="1" selected="selected">用户</option>
				</select>
			</div>
		</div>
		<div class="magb15 layui-col-md4 layui-col-xs12">
			<label class="layui-form-label">年龄</label>
			<div class="layui-input-block">
				<input type="text" id="age" name="age" class="layui-input userName" lay-verify="required|number" placeholder="请输入年龄">
			</div>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">渠道</label>
		<div class="layui-input-block">
			<select id="organizationId" name="organizationId" class="organizationId" lay-filter="organizationId">
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">角色</label>
		<div class="layui-input-block">
			<select id="roleIds" name="roleIds" xm-select="roleIds" xm-select-skin="normal">
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">电话</label>
		<div class="layui-input-block">
			<input type="text" id="phone" name="phone" class="layui-input userName" lay-verify="required|phone" placeholder="请输入电话">
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<label class="layui-form-label">用户状态</label>
		<div class="layui-input-block">
			<select id="status" name="status" class="userGrade" lay-filter="userGrade">
				<option value="0">正常</option>
				<option value="1">停用</option>
			</select>
		</div>
	</div>
	<div class="layui-form-item layui-row layui-col-xs12">
		<div class="layui-input-block">
			<button class="layui-btn layui-btn-sm" lay-submit="" lay-filter="addUser">立即添加</button>
			<button type="reset" class="layui-btn layui-btn-sm layui-btn-primary">取消</button>
		</div>
	</div>
</form>
<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
<script type="text/javascript" src="${path }/static/page/user/userAdd.js"></script>
</body>
</html>