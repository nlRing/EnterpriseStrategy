<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html class="loginHtml">
<head>
	<meta charset="utf-8">
	<title>登录--中小企业战略平台 1.0</title>
	<link rel="icon" href="${path }/static/favicon.ico">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
</head>
<body class="loginBody">
	<form class="layui-form" method="post" id="loginform">
		<div class="login_face"><img src="${path }/static/images/face.jpg" class="userAvatar"></div>
		<div class="layui-form-item input-item">
			<label for="userName">用户名</label>
			<input type="text" placeholder="请输入用户名" autocomplete="off" id="userName" class="layui-input" lay-verify="required">
		</div>
		<div class="layui-form-item input-item">
			<label for="password">密码</label>
			<input type="password" placeholder="请输入密码" autocomplete="off" id="password" class="layui-input" lay-verify="required">
		</div>
		<div class="layui-form-item input-item" id="imgCode">
			<label for="code">验证码</label>
			<input type="text" placeholder="请输入验证码" autocomplete="off" id="code" class="layui-input">
			<img id="captcha" alt="验证码" src="${path }/captcha.jpg" data-src="${path }/captcha.jpg?t=" style="vertical-align:middle;border-radius:4px;width:94.5px;height:36px;cursor:pointer;">
			<input class="rememberMe" type="hidden" id="rememberMe" name="rememberMe" value="1" />
		</div>
		<div class="layui-form-item">
			<button class="layui-btn layui-block" lay-filter="login" lay-submit>登录</button>
		</div>
	</form>
	<script type="text/javascript" src="${path }/static/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
	<script type="text/javascript" src="${path }/static/page/login/login.js"></script>
	<script type="text/javascript" src="${path }/static/js/cache.js"></script>
</body>
</html>