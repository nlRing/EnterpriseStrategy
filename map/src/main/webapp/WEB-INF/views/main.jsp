<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%@ include file="/commons/basejs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>首页--中小型企业后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/public.css" media="all" />
	<link rel="stylesheet" href="${path }/static/css/main.css" media="all" />
	<script type="text/javascript">
    	var userName = "<shiro:principal></shiro:principal>";
    </script>
</head>
<body class="childrenBody">
	<blockquote class="layui-elem-quote layui-bg-green">
		<div id="nowTime"></div>
	</blockquote>
	<div class="layui-row layui-col-space10 panel_box">
		<div class="panel layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg2">
			<a href="javascript:;" data-url="user/manager">
				<div class="panel_icon layui-bg-orange">
					<i class="layui-anim seraph icon-icon10" data-icon="icon-icon10"></i>
				</div>
				<div class="panel_word userAll">
					<span></span>
					<em>用户总数</em>
					<cite class="layui-hide">用户中心</cite>
				</div>
			</a>
		</div>
		<div class="panel layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg2">
			<a href="javascript:;" data-url="role/manager">
				<div class="panel_icon layui-bg-cyan">
					<i class="layui-anim layui-icon" data-icon="&#xe857;">&#xe857;</i>
				</div>
				<div class="panel_word outIcons">
					<span></span>
					<em>角色总数</em>
					<cite class="layui-hide">角色管理</cite>
				</div>
			</a>
		</div>
		<div class="panel layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg2">
			<a href="javascript:;">
				<div class="panel_icon layui-bg-blue">
					<i class="layui-anim seraph icon-clock"></i>
				</div>
				<div class="panel_word">
					<span class="loginTime"></span>
					<cite>上次登录时间</cite>
				</div>
			</a>
		</div>
	</div>
	<div class="row">
	    <div class="sysNotice col">
	        <div class="layui-collapse" >
	            <div class="layui-colla-item">
	                <h2 class="layui-colla-title" style="background-color: #ffffff;">地市企业分布统计</h2>
	                <div class="layui-colla-content layui-show" >
	                    <div id="cityChartId" style="height: 350px; margin: 0 auto;width: 100%;"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="sysNotice col">
	        <div class="layui-collapse">
	            <div class="layui-colla-item">
	                <h2 class="layui-colla-title" style="background-color: #ffffff;">行业类型分布统计</h2>
	                <div class="layui-colla-content layui-show" >
	                    <div id="enterpriseChartId" style="height: 350px; margin: 0 auto;width: 100%;"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="row">
	    <div class="sysNotice col">
	        <div class="layui-collapse" >
	            <div class="layui-colla-item">
	                <h2 class="layui-colla-title" style="background-color: #ffffff;">地市业务发展排名统计</h2>
	                <div class="layui-colla-content layui-show" >
	                    <div id="cityBusinessBarId" style="height: 350px; margin: 0 auto;width: 100%;"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="sysNotice col">
	        <div class="layui-collapse">
	            <div class="layui-colla-item">
	                <h2 class="layui-colla-title" style="background-color: #ffffff;">地市行业发展排名统计</h2>
	                <div class="layui-colla-content layui-show" >
	                    <div id="enterpriseBarId" style="height: 350px; margin: 0 auto;width: 100%;"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<script type="text/javascript" src="${path }/static/js/echarts.js"></script>
	<script type="text/javascript" src="${path }/static/js/macarons.js"></script>
	<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
	<script type="text/javascript" src="${path }/static/js/main.js"></script>
</body>
</html>