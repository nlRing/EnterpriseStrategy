<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>中小企业战略平台 1.0</title>
	<%@ include file="/commons/basejs.jsp" %>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${path }/static/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${path }/static/layui/css/map.css" media="all" />
	<link rel="stylesheet" href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
	<style type="text/css">
		body, html {width: 100%;height: 100%; margin:0;font-family:"微软雅黑";}
		#allmap{height:90%;width:100%;}
		.anchorBL{display:none;}
		 .BMap_bubble_title{  
        color:black;  
        font-size:13px;  
        font-weight: bold;  
        text-align:left;  
    }  
    .BMap_pop div:nth-child(1){  
        border-radius:7px 0 0 0;  
    }  
    .BMap_pop div:nth-child(3){  
        border-radius:0 7px 0 0;background:#ABABAB;;  
        /*background: #ABABAB;*/  
        width:23px;  
        width:0px;height;0px;  
	    }  
	    .BMap_pop div:nth-child(3) div{  
	        border-radius:7px;  
	    }  
	    .BMap_pop div:nth-child(5){  
	        border-radius:0 0 0 7px;  
	    }  
	    .BMap_pop div:nth-child(5) div{  
	        border-radius:7px;  
	    }  
	    .BMap_pop div:nth-child(7){  
	        border-radius:0 0 7px 0 ;  
	    }  
	    .BMap_pop div:nth-child div(7){  
	        border-radius:7px ;  
	    }  
	</style>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=orsN0xHVa8H0VxcMWW0uGf03"></script>
	<title>政企地图</title>
</head>
<body>
	<form class="layui-form">
		<blockquote class="layui-elem-quote">
		<div class="layui-form-item">
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="text" class="layui-input searchVal" placeholder="请输入搜索的企业名称" />
			</div>
			<a class="layui-btn search_btn" data-type="reload">搜索</a>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">城市选择:</label>
			<!-- 
			<div class="layui-input-inline">
				<select id="city" name="city" lay-filter="city">
					<option value="">请选择城市</option>
			        <option value="哈尔滨">哈尔滨</option>
			        <option value="齐齐哈尔">齐齐哈尔</option>
			        <option value="鸡西">鸡西</option>
			        <option value="鹤岗">鹤岗</option>
			        <option value="双鸭山">双鸭山</option>
			        <option value="大庆">大庆</option>
			        <option value="宜春">宜春</option>
			        <option value="佳木斯">佳木斯</option>
			        <option value="七台河">七台河</option>
			        <option value="牡丹江">牡丹江</option>
			        <option value="黑河">黑河</option>
			        <option value="绥化">绥化</option>
			        <option value="大兴安岭地区">大兴安岭地区</option>
				</select>
			</div>
			 -->
			<div class="layui-input-inline layui-form">
				<select id="province" name="province" lay-filter="province" class="province">
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="city" name="city" lay-filter="city" disabled>
					<option value="">请选择市</option>
				</select>
			</div>
			<div class="layui-input-inline layui-form">
				<select id="area" name="area" lay-filter="area" disabled>
					<option value="">请选择县/区</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">行业选择:</label>
			<div class="layui-input-inline">
				<select id="industry" name="industry" lay-filter="industry">
					<option value="">请选择行业</option>
			        <option value="公共管理、社会保障和社会组织">公共管理、社会保障和社会组织</option>
			        <option value="电力、热力燃气及水产和供应业">电力、热力燃气及水产和供应业</option>
			        <option value="水利、环境和公共设施管理业">水利、环境和公共设施管理业</option>
			        <option value="采矿业">采矿业</option>
			        <option value="建筑业">建筑业</option>
			        <option value="批发和零售业">批发和零售业</option>
			        <option value="交通运输、仓储和邮政业">交通运输、仓储和邮政业</option>
			        <option value="农、林、牧、渔业">农、林、牧、渔业</option>
			        <option value="制造业">制造业</option>
			        <option value="租赁和商务服务业">租赁和商务服务业</option>
			        <option value="科学研究和技术服务业">科学研究和技术服务业</option>
			        <option value="居民服务、修理和其他服务业">居民服务、修理和其他服务业</option>
			        <option value="信息传输、软件和信息技术服务业">信息传输、软件和信息技术服务业</option>
			        <option value="金融业">金融业</option>
			        <option value="房地产业">房地产业</option>
			        <option value="卫生和社会工作">卫生和社会工作</option>
			        <option value="教育">教育</option>
			        <option value="文化、体育和娱乐业">文化、体育和娱乐业</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">发展状态:</label>
			<div class="layui-input-inline">
				<select id="develop" name="develop" lay-filter="develop">
					<option value="0">全部</option>
			        <option value="1">已发展</option>
			        <option value="2">未发展</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">企业类型:</label>
			<div class="layui-input-inline">
				<select id="type" name="type" lay-filter="type">
					<option value="">全部</option>
			        <option value="党政军">党政军</option>
			        <option value="大企业">大企业</option>
			        <option value="金融">金融</option>
			        <option value="商客">商客</option>
			        <option value="高校">高校</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label" style="padding:9px 9px 15px 0px">企业规模:</label>
			<div class="layui-input-inline">
				<select id="scale" name="scale" lay-filter="scale">
					<option value="">全部</option>
			        <option value="大型企业">大型企业</option>
			        <option value="中型企业">中型企业</option>
			        <option value="小型企业">小型企业</option>
			        <option value="微型企业">微型企业</option>
				</select>
			</div>
		</div>
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal showEnterprise_btn">企业展现</a>
		</div>
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal showHeatmap_btn">热力图</a>
		</div>
		<div class="layui-inline">
			<a class="layui-btn layui-btn-danger batchDel">清除地图</a>
		</div>
		<div class="layui-inline">
			<a id="exportBtn" class="layui-btn layui-btn-normal layui-btn-disabled export_btn"><i class="layui-icon">&#xe601;</i>导出</a>
		</div>
		</div>
		</blockquote>
	</form>
	<div id="allmap"></div>
	<!-- 这里如果需要把子页面的高度调高，直接在这里添加height -->
	<div id="main"></div>
	<div id="con"></div>
	<script type="text/javascript" src="${path }/static/layui/layui.js"></script>
	<script type="text/javascript" src="${path }/static/layui/js/mapv.js"></script>
	<script type="text/javascript" src="${path }/static/layui/js/DrawingManager_min.js"></script>
	<script type="text/javascript" src="${path }/static/layui/js/map.js"></script>
</body>
</html>