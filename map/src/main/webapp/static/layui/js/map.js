/**
  项目JS主入口
  百度地图
**/
var map = new BMap.Map("allmap",{enableMapClick:false});            // 创建Map实例  
map.enableScrollWheelZoom(true);
map.centerAndZoom("哈尔滨",12);      // 初始化地图,用城市名设置地图中心点
map.setMapStyle({style:'midnight'});
var geoc = new BMap.Geocoder();
var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT});// 右下角，添加比例尺
var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //右上角，仅包含平移和缩放按钮
/*缩放控件type有四种类型:
BMAP_NAVIGATION_CONTROL_SMALL：仅包含平移和缩放按钮；BMAP_NAVIGATION_CONTROL_PAN:仅包含平移按钮；BMAP_NAVIGATION_CONTROL_ZOOM：仅包含缩放按钮*/

map.addControl(top_left_control);        
map.addControl(top_left_navigation);     
//map.addControl(top_right_navigation);

var mapvLayer = null;

//map.addControl(new BMap.MapTypeControl());   //添加地图类型控件

var enterpriseopts = {
		width : 300,     // 信息窗口宽度
		height: 225,     // 信息窗口高度
		title : "企业信息" , // 信息窗口标题
		enableMessage:true//设置允许信息窗发送短息
	   };

var tradingopts = {
		width : 300,     // 信息窗口宽度
		height: 225,     // 信息窗口高度
		title : "商圈信息" , // 信息窗口标题
		enableMessage:true//设置允许信息窗发送短息
	   };

var options = {
        splitList: { 
            1: 'rgba(255, 250, 50, 0.6)',
            //2: 'rgba(255, 50, 50, 0.6)'
            2: 'rgba(0, 255, 0, 0.6)'
        },
        globalCompositeOperation: 'lighter',
        size: 5,
        max: 30,
        draw: 'category',
        methods: {
            click: function (item) {
                $.ajax({
        			type : "GET",
        			url : "map/infoWindow",
        			data : {
        				"id" : item.id
        			},
        			dataType : "json",
        			success : function(data) {
						var startDiv = "<div id='info' style='height:180px;overflow:auto;margin-top:5;'>";
						var endDiv = "</div>";
						var info = "<b>省份："+data.province+"</b><br/><b>地市名称："+data.cityName+"<br/><b>公司名称："+data.companyName
						+"<br/><b>行业："+data.industry+"<br/><b>状态："+data.state+"<br/><b>法人："+data.legal
						+"<br/><b>注册资本："+data.registered+"<br/><b>成立日期："+data.establishment+"<br/><b>联系电话："+data.telephone
						+"<br/><b>地址："+data.address+"<br/><b>企业网址："+data.website+"<br/><b>企业邮箱："+data.email+"<br/><b>发展状态："+data.develop
						+"<br/><b>经营范围："+data.scope;
						var content = startDiv+info+endDiv;
						addClickHandler(content,data,enterpriseopts);
        			}
        		});
            }
        }
    }

var heatmapOptions = {
        size: 13,
        gradient: { '0.25': "rgb(0,0,255)", '0.55': "rgb(0,255,0)", '0.85': "yellow", '1.0': "rgb(255,0,0)"},
        max: 100,
        draw: 'heatmap'
    }

var removeMarker = function(e,ee,marker){
	map.removeOverlay(marker);
}

function getCenterPoint(path)
{
    var x = 0.0;
    var y = 0.0;
    for(var i=0;i<path.length;i++){
        x=x+ parseFloat(path[i].lng);
        y=y+ parseFloat(path[i].lat);
    }
    x=x/path.length;
    y=y/path.length;
    return new BMap.Point(x,y);
}

var overlays = [];
var ids = [];
//未发展的企业
var notDevelopids = new Array;
var polygoncomplete = function(e, overlay){
	for(var i = 0; i < overlays.length; i++){
        map.removeOverlay(overlays[i]);
    }
	overlays.length = 0;
	var province = $("#province").find("option:selected").val();
	var cityName = $("#city").find("option:selected").val();
	var industry = $("#industry").find("option:selected").val();
	var develop = $("#develop").find("option:selected").val();
	var scale = $("#scale").find("option:selected").val();
	var type = $("#type").find("option:selected").val();
	var point = getCenterPoint(overlay.getPath());
	var jsonData = JSON.stringify(overlay.getPath());// 转成JSON格式
	var result = $.parseJSON(jsonData);// 转成JSON对象
	overlays.push(overlay);
	$.ajax({
		type : "POST",
		url : "map/countPolygon",
		data : {
			"province" : province,
			"cityName" : cityName,
			"industry" : industry,
			"develop" : develop,
			"scale" : scale,
			"type" : type,
			"data" : jsonData
		},
		dataType : "json",
		success : function(result) {
			var user = result.user;
			var msg = result.msg;
			var product = result.product;
			result = result.data;
			for(var i = 0; i < result.length; i++){
				var id = result[i].id;
				var count = result[i].count;
				if(count == '2'){
					notDevelopids.push(id);
				}
				ids.push(id);
			}
			if(result.length==0){
				$("#exportBtn").addClass("layui-btn-disabled");
				layer.msg('该区域没有企业数据,请重新框选!');
			}else{
				$("#exportBtn").removeClass("layui-btn-disabled");
				var dataSet = new mapv.DataSet(result);
				mapvLayer = new mapv.baiduMapLayer(map, dataSet, options);
			}
			var optionhtml = '';
			for(var i = 0; i < product.length; i++){
				optionhtml = optionhtml+"<option value="+product[i].id+">"+product[i].name+"</option>";
			}
			var userhtml = '';
			for(var i = 0; i < user.length; i++){
				userhtml = userhtml+"<option value="+user[i].id+">"+user[i].name+"</option>";
			}
			var startDiv = "<div id='info' style='height:180px;'>";
			var endDiv = "</div>";
			var info = "<b>已发展企业数量："+msg.isdevelop+"个</b><br/><b>未发展企业数量："+msg.nodevelop+"个<br/>";
			var productDiv = "<div class='layui-inline'>"+
						"<label class='layui-form-label'><b>主推产品:</b></label>"+
						"<div class='layui-input-inline'>"+
							"<b><select id='product' name='product' lay-filter='product'>"+
							optionhtml+
							"</select>"+
						"</div>"+
					"</div></b><br/>";
			var userDiv = "<div class='layui-inline'>"+
								"<label class='layui-form-label'><b>业务员:</b></label>"+
								"<div class='layui-input-inline'>"+
									"<b><select id='user' name='user' lay-filter='user'>"+
									userhtml+
									"</select>"+
								"</div>"+
							"</div></b><br/>";
			var subDiv = "<div class='layui-form-item layui-row layui-col-xs12'>"+
							"<div class='layui-input-block'>"+
								"<button class='layui-btn layui-btn-sm' lay-submit='' onclick='fun(notDevelopids)'>立即分配</button>"+
							"</div>"+
						"</div>";
			var content = startDiv+info+productDiv+subDiv+endDiv;
			//var content = startDiv+info+productDiv+userDiv+subDiv+endDiv;
			//var point = new BMap.Point(longitude, latitude);
			var infoWindow = new BMap.InfoWindow(content,tradingopts);  // 创建信息窗口对象 
			map.openInfoWindow(infoWindow,point); //开启信息窗口
		}
	});
}
var circlecomplete = function(e, overlay){
	for(var i = 0; i < overlays.length; i++){
        map.removeOverlay(overlays[i]);
    }
	overlays.length = 0;
	if(mapvLayer != null){
		mapvLayer.destroy();
	}
	//创建右键菜单
	var markerMenu=new BMap.ContextMenu();
	markerMenu.addItem(new BMap.MenuItem('删除',removeMarker.bind(e)));
	e.addContextMenu(markerMenu);
    overlays.push(overlay);
    var radius = e.getRadius();
    var center = e.getCenter();
    drawingManager.close();
    var province = $("#province").find("option:selected").val();
	var cityName = $("#city").find("option:selected").val();
	var industry = $("#industry").find("option:selected").val();
	var develop = $("#develop").find("option:selected").val();
	var scale = $("#scale").find("option:selected").val();
	var type = $("#type").find("option:selected").val();
    //这里根据圆的半径来判断地市，然后返回该地市的所有点位数据，然后进行判断是否在圆内，如果在圆内，则导出这些数据
    geoc.getLocation(center, function(rs){
    	ids = [];
    	notDevelopids = new Array;
//		var addComp = rs.addressComponents;
//		var city = addComp.city;
//		var province = addComp.province;
		var longitude = center.lng;
		var latitude = center.lat;
		$.ajax({
			type : "POST",
			url : "map/countDistance",
			data : {
				"province" : province,
				"cityName" : cityName,
				"industry" : industry,
				"longitude" : longitude,
				"latitude" : latitude,
				"develop" : develop,
				"scale" : scale,
				"type" : type,
				"radius" : radius
			},
			dataType : "json",
			success : function(result) {
				var user = result.user;
				var msg = result.msg;
				var product = result.product;
				result = result.data;
				for(var i = 0; i < result.length; i++){
					var id = result[i].id;
					var count = result[i].count;
					if(count == '2'){
						notDevelopids.push(id);
					}
					ids.push(id);
				}
				if(result.length==0){
					$("#exportBtn").addClass("layui-btn-disabled");
					layer.msg('该区域没有企业数据,请重新框选!');
				}else{
					$("#exportBtn").removeClass("layui-btn-disabled");
					var dataSet = new mapv.DataSet(result);
					mapvLayer = new mapv.baiduMapLayer(map, dataSet, options);
				}
				var optionhtml = '';
				for(var i = 0; i < product.length; i++){
					optionhtml = optionhtml+"<option value="+product[i].id+">"+product[i].name+"</option>";
				}
				var userhtml = '';
				for(var i = 0; i < user.length; i++){
					userhtml = userhtml+"<option value="+user[i].id+">"+user[i].name+"</option>";
				}
				var startDiv = "<div id='info' style='height:180px;'>";
				var endDiv = "</div>";
				var info = "<b>已发展企业数量："+msg.isdevelop+"个</b><br/><b>未发展企业数量："+msg.nodevelop+"个<br/>";
				var productDiv = "<div class='layui-inline'>"+
							"<label class='layui-form-label'><b>主推产品:</b></label>"+
							"<div class='layui-input-inline'>"+
								"<b><select id='product' name='product' lay-filter='product'>"+
								optionhtml+
								"</select>"+
							"</div>"+
						"</div></b><br/>";
				var userDiv = "<div class='layui-inline'>"+
									"<label class='layui-form-label'><b>业务员:</b></label>"+
									"<div class='layui-input-inline'>"+
										"<b><select id='user' name='user' lay-filter='user'>"+
										userhtml+
										"</select>"+
									"</div>"+
								"</div></b><br/>";
				var subDiv = "<div class='layui-form-item layui-row layui-col-xs12'>"+
								"<div class='layui-input-block'>"+
									"<button class='layui-btn layui-btn-sm' lay-submit='' onclick='fun(notDevelopids)'>立即分配</button>"+
								"</div>"+
							"</div>";
				//var content = startDiv+info+productDiv+userDiv+subDiv+endDiv;
				var content = startDiv+info+productDiv+subDiv+endDiv;
				var point = new BMap.Point(longitude, latitude);
				var infoWindow = new BMap.InfoWindow(content,tradingopts);  // 创建信息窗口对象 
				map.openInfoWindow(infoWindow,point); //开启信息窗口
			}
		});
	});
};

function fun(notDevelopids){
	var product = $("#product").val();
	//var user = $("#user").val();
	if(notDevelopids.length == 0){
		layer.msg('该区域没有需要分配企业数据,请重新框选!');
	}
	$.ajax({
		type : "POST",
		url : "task/addBatchTask",
		data : {
			"primaryP" : product,
			//"userId" : user,
			"ids" : notDevelopids
		},
		dataType : "json",
		success : function(result) {
			//console.log(basePath + '/order/manager');
			window.location.href = basePath + '/order/manager';
			//console.log(result);
		}
	});
}

var styleOptions = {
    strokeColor:"red",    //边线颜色。
    fillColor:"red",      //填充颜色。当参数为空时，圆形将没有填充效果。
    strokeWeight: 3,       //边线的宽度，以像素为单位。
    strokeOpacity: 0.8,	   //边线透明度，取值范围0 - 1。
    fillOpacity: 0.6,      //填充的透明度，取值范围0 - 1。
    strokeStyle: 'solid' //边线的样式，solid或dashed。
}

//实例化鼠标绘制工具
var drawingManager = new BMapLib.DrawingManager(map, {
    isOpen: false, //是否开启绘制模式
    enableDrawingTool: true, //是否显示工具栏
    drawingToolOptions: {
        anchor: BMAP_ANCHOR_TOP_RIGHT, //位置
        //offset: new BMap.Size(60, 5), //偏离值
        offset: new BMap.Size(5, 5), //偏离值
        drawingModes:[
                      BMAP_DRAWING_CIRCLE,BMAP_DRAWING_POLYGON
                  ]
    },
    circleOptions: styleOptions, //圆的样式
    polygonOptions: styleOptions //多边形的样式
});

//添加鼠标绘制工具监听事件，用于获取绘制结果
drawingManager.addEventListener('circlecomplete', circlecomplete);
drawingManager.addEventListener('polygoncomplete', polygoncomplete);

function addClickHandler(content,data,opts){
	openInfo(content,data,opts);
};

function openInfo(content,data,opts){
	var point = new BMap.Point(data.longitude, data.latitude);
	var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
	map.openInfoWindow(infoWindow,point); //开启信息窗口
};


/**
  项目JS主入口
  以依赖Layui的layer和form模块为例
**/    
layui.config({
	base : "static/js/"
}).extend({
    "address" : "mapaddress"
})
layui.use(['form','element','layer','jquery','address'],function(){
	var form = layui.form,
	layer = layui.layer,
	element = layui.element;
	$ = layui.jquery;
	var address = layui.address;
	
	//获取省信息
	address.provinces();
	
//	form.on('select(city)', function(data){
//		if(mapvLayer != null){
//			mapvLayer.destroy();
//		}
//		//var cityID = data.value;
//		var cityName = $("#city").find("option:selected").text();
//		form.render('select'); //这个很重要
//		map.centerAndZoom(cityName,12);
//	});
	
	function showFbusinessinfo(e){
		var point = e.point;
		var content = point.content;
		var point = new BMap.Point(point.lng, point.lat);
		var infoWindow = new BMap.InfoWindow(content,fbusinessopts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口	
	}
	
	//查询
	$(".search_btn").click(function(){
//		var local = new BMap.LocalSearch(map, {
//			renderOptions:{map: map}
//		});
//		var text = $(".searchVal").val();
//		if(text != ''){
//			local.search(text);
//		}
		if(mapvLayer != null){
			mapvLayer.destroy();
		}
		var data = [];
		var companyName = $(".searchVal").val().trim();
		$.ajax({
			type : "POST",
			url : "map/showByName",
			data : {
				"companyName" : companyName
			},
			dataType : "json",
			success : function(result) {
				if(result.length==0){
					layer.msg('该区域没有企业数据,请重新查询!');
				}else{
					var dataSet = new mapv.DataSet(result);
					var longitude = result[0].geometry.coordinates[0];
					var latitude = result[0].geometry.coordinates[1];
					var new_point = new BMap.Point(longitude,latitude);
					map.setCenter(new_point);
					mapvLayer = new mapv.baiduMapLayer(map, dataSet, options);
				}
			}
		});
	});
	
	//显示所有企业
	$(".showEnterprise_btn").click(function(){
		if(mapvLayer != null){
			mapvLayer.destroy();
		}
		var data = [];
		var province = $("#province").find("option:selected").text();
		var cityName = $("#city").find("option:selected").text();
		var industry = $("#industry").find("option:selected").text();
		var develop = $("#develop").find("option:selected").val();
		var type = $("#type").find("option:selected").val();
		var scale = $("#scale").find("option:selected").val();
		if(cityName != '请选择城市' && industry != '请选择行业'){
			$.ajax({
				type : "POST",
				url : "map/show",
				data : {
					"province" : province,
					"cityName" : cityName,
					"industry" : industry,
					"develop" : develop,
					"type" : type,
					"scale" : scale
				},
				dataType : "json",
				success : function(result) {
					if(result.length==0){
						layer.msg('该区域没有企业数据,请重新查询!');
					}else{
						var dataSet = new mapv.DataSet(result);
						mapvLayer = new mapv.baiduMapLayer(map, dataSet, options);
					}
				}
			});
		}else{
			layer.msg('请选择城市和行业！', {icon: 6});
		}
	});
	
	//展示热力图
	$(".showHeatmap_btn").click(function(){
		if(mapvLayer != null){
			mapvLayer.destroy();
		}
		var data = [];
		var cityName = $("#city").find("option:selected").text();
		var industry = $("#industry").find("option:selected").text();
		if(cityName != '请选择城市' && industry != '请选择行业'){
			$.ajax({
				type : "POST",
				url : "map/show",
				data : {
					"cityName" : cityName,
					"industry" : industry
				},
				dataType : "json",
				success : function(result) {
					if(result.length==0){
						layer.msg('该区域没有企业数据,请重新查询!');
					}else{
						var dataSet = new mapv.DataSet(result);
						mapvLayer = new mapv.baiduMapLayer(map, dataSet, heatmapOptions);
					}
				}
			});
		}else{
			layer.msg('请选择城市和行业！', {icon: 6});
		}
	});
	
	//清除地图
	$(".batchDel").click(function(){
		if(mapvLayer != null){
			mapvLayer.destroy();
		}
		for(var i = 0; i < overlays.length; i++){
            map.removeOverlay(overlays[i]);
        }
		overlays.length = 0;
	});
	
	//导出
    $(".export_btn").click(function(){
		window.open("test/downData?ids="+ids);
    });
});