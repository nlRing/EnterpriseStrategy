var form, $;
layui.config({
    base : "../static/js/"
}).extend({
    "address" : "address"
})
layui.use(['form','layer','table','upload','laytpl','address'],function(){
    	form = layui.form;
        $ = layui.jquery;
        var layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        upload = layui.upload,
        table = layui.table,
        address = layui.address;
        
    //获取省信息
    address.provinces();
    
	//上传
    var uploadInst = upload.render({
      elem: '#upload' //绑定元素
      ,url: '../product/fileUpload/' //上传接口
      ,method: 'post'
      ,accept : "file"
	  ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
		  layer.load(); //上传loading
	  }
      ,done: function(res, index, upload){
    	layer.closeAll('loading'); //关闭loading
        //上传完毕回调
    	layer.msg('上传成功');
      }
      ,error: function(){
        //请求异常回调
      }
    });
	
	//企业列表
    var tableIns = table.render({
        elem: '#gridList',
        method: 'post',
        url : '../map/dataGrid',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "gridListTable",
        cols : [[
            {field: 'id', title: '序号', minWidth:50, align:"center"},
            {field: 'province', title: '省份', minWidth:50, align:"center"},
            {field: 'cityName', title: '地市名称', minWidth:50, align:"center"},
            {field: 'companyName', title: '公司名称', minWidth:100, align:"center"},
            {field: 'industry', minWidth:100, title: '行业', align:'center'},
            {field: 'state', minWidth:50, title: '状态', align:'center'},
            {field: 'legal', minWidth:50, title: '法人', align:'center'},
            {field: 'registered', minWidth:50, title: '注册资本', align:'center'},
            {field: 'telephone', minWidth:50, title: '联系电话', align:'center'},
            {field: 'address', minWidth:100, title: '企业地址',  align:'center'},
            {field: 'website', minWidth:100, title: '企业网址',  align:'center'},
            {field: 'email', minWidth:80, title: '企业邮箱',  align:'center'},
            {field: 'develop', minWidth:80, title: '发展状态',  align:'center',templet:function(d){
                if(d.develop == "0"){
                    return "全部";
                }else if(d.develop == "1"){
                    return "已发展";
                }else if(d.develop == "2"){
                    return "未发展";
                }else{
                	return d.develop;
                }
            }},
            {field: 'scope', minWidth:100, title: '经营范围',  align:'center'},
            {title: '操作', minWidth:100, templet:'#gridListBar',fixed:"right",align:"center"}
        ]]
    });
    
//    form.on('select(city)', function(data){
//		var cityName = $("#city").find("option:selected").text();
//		form.render('select'); //这个很重要
//		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
//    	setTimeout(function(){
//    		table.reload("gridListTable",{
//                page: {
//                    curr: 1 //重新从第 1 页开始
//                },
//                where: {
//                	"cityName": cityName  //搜索的关键字
//                }
//            });
//			layer.close(index);
//		},2000);
//	});
    
    form.on('select(industry)', function(data){
		var industry = $("#industry").find("option:selected").text();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"industry": industry  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});
    
    form.on('select(develop)', function(data){
		var develop = $("#develop").find("option:selected").val();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"develop": develop  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});
    
    form.on('select(type)', function(data){
		var type = $("#type").find("option:selected").val();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"type": type  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});
    
    form.on('select(scale)', function(data){
		var scale = $("#scale").find("option:selected").val();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"scale": scale  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});

    //搜索
    $(".search_btn").on("click",function(){
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if($(".searchVal").val() != ''){
        	setTimeout(function(){
        		table.reload("gridListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	companyName: $(".searchVal").val()  //搜索的关键字
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });

    //导出
    $(".export_btn").click(function(){
    	var cityName = $("#city").find("option:selected").text();
		var industry = $("#industry").find("option:selected").text();
		window.open("../test/down?cityName="+cityName+"&industry="+industry);
    });
    
    //数据模板
    $(".template_btn").click(function(){
		window.open("../test/downTmp");
    });
    
    //列表操作
    table.on('tool(gridList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
        	editGrid(data);
        }
    });
    
    //编辑企业
    function editGrid(edit){
        var index = layui.layer.open({
            title : "编辑企业",
            type : 2,
            content : "../map/editGrid?id=" + edit.id,
            success : function(layero, index){
            	//alert(JSON.stringify(edit));
                var body = layui.layer.getChildFrame('body', index);
                body.find(".industry").val(edit.industry);  //行业
                body.find(".develop").val(edit.develop);  //发展状态
                body.find(".type").val(edit.type);  //企业状态
                body.find(".scale").val(edit.scale);  //企业规模
                body.find(".scope").text(edit.scope);    //经营范围
                form.render();
                setTimeout(function(){
                    layui.layer.tips('点击此处返回企业列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
})
