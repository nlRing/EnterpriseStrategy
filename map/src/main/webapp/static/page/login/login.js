layui.use(['form','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer
        $ = layui.jquery;

    // 验证码
    $("#captcha").click(function() {
        var $this = $(this);
        var url = $this.data("src") + new Date().getTime();
        $this.attr("src", url);
    });
    
    //登录按钮
    form.on("submit(login)",function(data){
    	var username=$("#userName").val().trim();
		var password=$("#password").val().trim();
		var captcha=$("#code").val().trim();
		var rememberMe=$("#rememberMe").val();
		$.ajax({
			type : "POST",
			url : basePath+"/login",
			data : {
				"username" : username,
				"password" : password,
				"captcha" : captcha,
				"rememberMe" : rememberMe
			},
			dataType : "json",
			success : function(jsonObject) {
				if (jsonObject.success) {
					window.location.href = basePath + '/index';
				} else {
					// 刷新验证码
					$("#captcha")[0].click();
					layer.msg(jsonObject.msg);
				}
			},
			error : function(data) {
                alert(data);
            }
		});
		return false;
    });
    
    //表单输入效果
    $(".loginBody .input-item").click(function(e){
        e.stopPropagation();
        $(this).addClass("layui-input-focus").find(".layui-input").focus();
    })
    $(".loginBody .layui-form-item .layui-input").focus(function(){
        $(this).parent().addClass("layui-input-focus");
    })
    $(".loginBody .layui-form-item .layui-input").blur(function(){
        $(this).parent().removeClass("layui-input-focus");
        if($(this).val() != ''){
            $(this).parent().addClass("layui-input-active");
        }else{
            $(this).parent().removeClass("layui-input-active");
        }
    })
})
