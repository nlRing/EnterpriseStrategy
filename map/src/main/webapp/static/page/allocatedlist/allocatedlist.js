layui.config({
    base : "../static/js/"
}).extend({
    "address" : "orderaddress"
})
layui.use(['form','layer','table','jquery','address','tablePlug','laydate'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        table = layui.table,
        $ = layui.jquery,
        tablePlug = layui.tablePlug,
        laydate = layui.laydate,
        address = layui.address;
    
    //获取省信息
    address.provinces();
    
    var submitTime;
    //日期范围
    laydate.render({
      elem: '#release',
      range: true,
      done : function(value, date, endDate){
          submitTime = value.toString();
      }
    });
    
    //企业列表
    var tableIns = table.render({
    	 elem: '#gridList',
         method: 'post',
         url : '../allocatedlist/dataGrid',
         cellMinWidth : 95,
         page : true,
         loading: true,
         sortType: 'server', 
         id: 'gridListTable',
         cols : [[
             {field: 'id', title: '序号', minWidth:50, align:"center"},
             {field: 'orderName', title: '订单名称', minWidth:50, align:"center"},
             {field: 'allocatedName', title: '批次名称', minWidth:50, align:"center"},
             {field: 'city', title: '地市', minWidth:100, align:"center"},
             {field: 'country', minWidth:100, title: '区县', align:'center'},
             {field: 'employeeId', minWidth:50, title: '录入工号', align:'center'},
             {title: '操作', minWidth:100, templet:'#gridListBar',fixed:"right",align:"center"}
         ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	var orderName = $("#orderName").val().trim();
    	var allocateName = $("#allocateName").val().trim();
    	var creater = $("#creater").val();
    	var city = $("#city").find("option:selected").val();
    	var area = $("#area").find("option:selected").val();
    	var searchVal = ''+orderName+allocateName+city+area+creater;
        if(searchVal != ''){
        	setTimeout(function(){
        		table.reload("gridListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	orderName: orderName,  //搜索的关键字
                    	allocateName: allocateName, 
                    	creater: creater,
                    	city: city,
                    	country: area,
                    	submitTime: submitTime
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
  
    //选择任务
    function taskDetails(data){
    	$('#allocateId').val(data.id);
    	var index = layui.layer.open({
            title : "营销推广",
            type : 2,
            content : "../allocatedlist/taskDetails",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回工单添加页面', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
         layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    } 
   
  //列表操作
    table.on('tool(gridList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'details'){ //订单任务列表
        	taskDetails(data);
        }else if(layEvent === 'del'){ //删除
    	var olddata = table.cache.gridListTable;
    	olddata.splice(obj.tr.data('index'),1);
    	tableIns.reload({data:olddata});
        }
    });
})