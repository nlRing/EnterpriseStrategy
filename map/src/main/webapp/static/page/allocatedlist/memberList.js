layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //用户列表
    var tableMen = table.render({
        elem: '#userList',
        method: 'post',
        url : '../user/menmberList',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'createTime' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "userListTable",
        cols : [[
        	{field: 'id', title: 'ID', minWidth:50, align:"center"},
        	{field: 'employeeId', title: '工号', minWidth:100, align:"center"},
            {field: 'loginName', title: '用户名', minWidth:100, align:"center"},
            {field: 'name', title: '姓名', minWidth:100, align:"center"},
            {field: 'createTime', title: '创建时间', minWidth:150, align:"center"},
            {field: 'sex', minWidth:50, title: '用户性别', align:'center',templet:function(d){
            	return d.sex == "0" ? "男" : "女";
            }},
            {field: 'age', minWidth:50, title: '年龄', align:'center'},
            {field: 'phone', title: '电话', align:'center'},
            {field: 'rolesList', title: '角色', align:'center'},
            {field: 'userType', title: '用户类型', align:'center',templet:function(d){
                if(d.userType == "0"){
                    return "管理员";
                }else if(d.userType == "1"){
                    return "用户";
                }else{
                    return "未知类型";
                }
            }},
            {field: 'status', title: '用户状态',  align:'center',templet:function(d){
            	return d.status == "0" ? "正常使用" : "限制使用";
            }},
            {title: '操作', minWidth:175, templet:'#userListBar',fixed:"right",align:"center"}
        ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if($(".searchVal").val() != ''){
        	setTimeout(function(){
        		table.reload("userListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	name: $(".searchVal").val()  //搜索的关键字
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableMen.reload();
        	layer.close(index);
        }
    });


    //列表操作
    table.on('tool(userList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'choose'){ //编辑
        	parent.$('#employeeId').val(data.employeeId);
        	parent.$('#userId').val(data.id);
        }
    });
})
