layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //任务列表
    var tableIns = table.render({
        elem: '#taskList',
        method: 'post',
        url : '../allocatedlist/taskPage?allocatedId='+parent.$('#allocateId').val(),
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "taskListTable",
        cols : [[
        	 {field: 'id', title: 'id', minWidth:150, align:"center"},
        	 {field: 'province', title: '省份', minWidth:50, align:"center"},
             {field: 'cityName', title: '地市名称', minWidth:50, align:"center"},
             {field: 'companyName', title: '公司名称', minWidth:100, align:"center"},
             {field: 'industry', minWidth:100, title: '行业', align:'center'},
             {field: 'state', minWidth:50, title: '状态', align:'center'},
             {field: 'legal', minWidth:50, title: '法人', align:'center'},
             {field: 'registered', minWidth:50, title: '注册资本', align:'center'},
             {field: 'telephone', minWidth:50, title: '联系电话', align:'center'},
             {field: 'address', minWidth:100, title: '企业地址',  align:'center'},
             {field: 'website', minWidth:100, title: '企业网址',  align:'center'},
             {field: 'email', minWidth:80, title: '企业邮箱',  align:'center'},
             {field: 'develop', minWidth:80, title: '发展状态',  align:'center'},
             {field: 'scope', minWidth:100, title: '经营范围',  align:'center'},
            {field: 'primaryP', title: '主推产品', minWidth:100, align:"center"},
            {field: 'hotP', title: '热推产品', minWidth:100, align:"center"},
            {field: 'employeeId', title: '执行人', minWidth:100, align:"center"},    
            {title: '操作', minWidth:100, templet:'#taskListBar',fixed:"right",align:"center"}
            ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	console.log($(".searchVal").val());
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if($(".searchVal").val() != ''){
        	setTimeout(function(){
        		table.reload("taskListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	"companyName": $(".searchVal").val()  //搜索的关键字           
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });
    
    //搜索
    $(".share").on("click",function(){
    	 var index = layui.layer.open({
             title : "分享产品",
             type : 2,
             content : "../allocatedlist/sharePage?allocatedId=" + parent.$('#allocateId').val(),
             success : function(layero, index){
                 var body = layui.layer.getChildFrame('body', index);
                 setTimeout(function(){
                     layui.layer.tips('点击此处返回产品列表', '.layui-layer-setwin .layui-layer-close', {
                         tips: 3
                     });
                 },500)
             }
         })
         layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    });
    
    //编辑企业
    function editGrid(edit){
        var index = layui.layer.open({
            title : "编辑企业",
            type : 2,
            content : "../map/editGrid?id=" + edit.enterpriseId,
            success : function(layero, index){
            	//alert(JSON.stringify(edit));
                var body = layui.layer.getChildFrame('body', index);
                if (typeof(edit.picture) != "undefined"){
                	body.find(".linkLogo").css("background","#fff");
                	console.log(edit.picture+'-----');
                	body.find(".linkLogoImg").attr("src",edit.picture);
                }
                body.find(".industry").val(edit.industry);  //行业
                body.find(".develop").val(edit.develop);  //发展状态
                body.find(".type").val(edit.type);  //企业状态
                body.find(".scale").val(edit.scale);  //企业规模
                body.find(".scope").text(edit.scope);    //经营范围
                form.render();
                setTimeout(function(){
                    layui.layer.tips('点击此处返回企业列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    //列表操作
    table.on('tool(taskList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
       if(layEvent === 'choose'){ //删除
    	   chooseTask(data);
        }
       if(layEvent === 'edit'){ //编辑
       	editGrid(data);
       }
    });
})
