var form, $;
layui.config({
    base : "../static/js/"
}).extend({
    "address" : "address"
})
layui.use(['form','layer','table','upload','laytpl','address'],function(){
    	form = layui.form;
        $ = layui.jquery;
        var layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        upload = layui.upload,
        table = layui.table,
        address = layui.address;
        
    //获取省信息
    address.provinces();
	
	//企业列表
    var tableIns = table.render({
        elem: '#gridList',
        method: 'post',
        url : '../task/enterpriseGrid?primaryP='+parent.$('#primaryP').val()+'&hotP='+parent.$('#hotP').val(),
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "gridListTable",
        cols : [[
            {field: 'id', title: '序号', minWidth:50, align:"center"},
            {field: 'province', title: '省份', minWidth:50, align:"center"},
            {field: 'cityName', title: '地市名称', minWidth:50, align:"center"},
            {field: 'companyName', title: '公司名称', minWidth:100, align:"center"},
            {field: 'industry', minWidth:100, title: '行业', align:'center'},
            {field: 'state', minWidth:50, title: '状态', align:'center'},
            {field: 'legal', minWidth:50, title: '法人', align:'center'},
            {field: 'registered', minWidth:50, title: '注册资本', align:'center'},
            {field: 'telephone', minWidth:50, title: '联系电话', align:'center'},
            {field: 'address', minWidth:100, title: '企业地址',  align:'center'},
            {field: 'website', minWidth:100, title: '企业网址',  align:'center'},
            {field: 'email', minWidth:80, title: '企业邮箱',  align:'center'},
            {field: 'develop', minWidth:80, title: '发展状态',  align:'center',templet:function(d){
                if(d.develop == "0"){
                    return "全部";
                }else if(d.develop == "1"){
                    return "已发展";
                }else if(d.develop == "2"){
                    return "未发展";
                }else{
                	return d.develop;
                }
            }},
            {field: 'scope', minWidth:100, title: '经营范围',  align:'center'},
            {title: '操作', minWidth:100, templet:'#gridListBar',fixed:"right",align:"center"}
        ]]
    });
    
    form.on('select(industry)', function(data){
		var industry = $("#industry").find("option:selected").text();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"industry": industry  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});
    
    form.on('select(develop)', function(data){
		var develop = $("#develop").find("option:selected").val();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"develop": develop  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});
    
    form.on('select(type)', function(data){
		var type = $("#type").find("option:selected").val();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"type": type  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});
    
    form.on('select(scale)', function(data){
		var scale = $("#scale").find("option:selected").val();
		form.render('select'); //这个很重要
		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	setTimeout(function(){
    		table.reload("gridListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                	"scale": scale  //搜索的关键字
                }
            });
			layer.close(index);
		},2000);
	});

    //搜索
    $(".search_btn").on("click",function(){
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if($(".searchVal").val() != ''){
        	setTimeout(function(){
        		table.reload("gridListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	companyName: $(".searchVal").val()  //搜索的关键字
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });
    
    //选取企业
    function chooseEnterprise(choose){
    	var fg = true;
    	var oldData = new Array();
    	var enterpriseIds;
    	var len;
    	if(parent.table.cache.gridListTable){
    		oldData = parent.table.cache.gridListTable;
    	}
        oldData.forEach(item=>{
            if(item.companyName==choose.companyName){
            	fg = false
                layer.alert('该企业已添加！');
            }
            len++;
        })
        if(fg){
        	if(oldData.length==10){
        		layer.alert("只能选择10个企业！");
        	}else{
        		oldData.push(choose);
                parent.table.reload('gridListTable',{data:oldData});
//                enterpriseIds=parent.$('#enterpriseIds').val()?parent.$('#enterpriseIds').val()+';'+choose.id:choose.id	
//                parent.$('#enterpriseIds').val(enterpriseIds);		
        	}
        }
    }
    
    //列表操作
    table.on('tool(gridList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'choose'){ //编辑
        	chooseEnterprise(data);
        }
    });
})
