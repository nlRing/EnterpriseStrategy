layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //任务列表
    var tableIns = table.render({
        elem: '#taskList',
        method: 'post',
        url : '../task/dataGrid',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "taskListTable",
        cols : [[
        	 {field: 'id', title: 'id', minWidth:150, align:"center"},
            {field: 'companyName', title: '企业名称', minWidth:100, align:"center"},
            {field: 'province', title: '省份', minWidth:100, align:"center"},
            {field: 'city', title: '地市', minWidth:100, align:"center"},
            {field: 'primaryP', title: '主推产品', minWidth:100, align:"center"},
            {field: 'hotP', title: '热推产品', minWidth:100, align:"center"},
            {field: 'employeeId', title: '创建人工号', minWidth:100, align:"center"},
            {field: 'createTime', title: '创建时间', minWidth:100, align:"center"},
            {field: 'type', title: '类型', minWidth:100, align:"center",templet:function(d){
                if(d.type == "0"){
                    return "未分配";
                }else if(d.type == "1"){
                    return "已分配";
                }else if(d.type == "2"){
                	return "导入";
                }
            }},
            {title: '操作', minWidth:175, templet:'#taskListBar',fixed:"right",align:"center"}
        ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	var companyName = $("#companyName").val()
    	var type = $("#type").val()
    	var searchVal = '' + companyName + type;
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if(searchVal != ''){
        	setTimeout(function(){
        		table.reload("taskListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	"companyName": companyName,  //搜索的关键字
                    	"type": type
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });

    //添加任务
    function addtask(){
        var index = layui.layer.open({
            title : "添加任务",
            type : 2,
            content : "../task/addPage",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回任务列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    //编辑任务
    function edittask(edit){
        var index = layui.layer.open({
            title : "编辑任务",
            type : 2,
            content : "../task/editPage?id=" + edit.id,
            success : function(layero, index){
            	//alert(JSON.stringify(edit));
                var body = layui.layer.getChildFrame('body', index);
                body.find(".name").val(edit.name);  //性别
                body.find(".description").val(edit.description);  //用户类型
                body.find(".seq").val(edit.seq);  //排序
                body.find(".status").val(edit.status);  //用户状态
                //body.find(".taskIds").val(edit.tasksList);  //角色
                form.render();
                setTimeout(function(){
                    layui.layer.tips('点击此处返回角色列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    $(".addNews_btn").click(function(){
        addtask();
    })

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('userListTable'),
            data = checkStatus.data,
            newsId = [];
        if(data.length > 0) {
            for (var i in data) {
                newsId.push(data[i].newsId);
            }
            layer.confirm('确定删除选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
                // $.get("删除文章接口",{
                //     newsId : newsId  //将需要删除的newsId作为参数传入
                // },function(data){
                tableIns.reload();
                layer.close(index);
                // })
            })
        }else{
            layer.msg("请选择需要删除的用户");
        }
    })

    //列表操作
    table.on('tool(taskList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'edit'){ //编辑
        	edittask(data);
        }else if(layEvent === 'usable'){ //启用禁用
        	var id = data.id;
        	granttaskFun(id);
        }else if(layEvent === 'del'){ //删除
        	var id = data.id;
        	if(data.type!='0'){
        		layer.alert('已分配的任务无法删除！');
        	}else{
            layer.confirm('确定删除此任务？',{icon:3, title:'提示信息'},function(index){
    			$.ajax({
      			type : "POST",
      			url : "../task/delete",
      			data : {
      				"id" : id
      			},
      			success : function() {
      				top.layer.msg("任务删除成功！");
      	 			tableIns.reload();
                    layer.close(index);
      			}
    			});
            });
        	}
        }
    });
})
