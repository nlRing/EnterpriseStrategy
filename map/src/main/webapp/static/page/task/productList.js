layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //产品列表
    var tableIns = table.render({
        elem: '#productList',
        method: 'post',
        url : '../product/dataGrid',
        //url : '../static/json/pic.json',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'createTime' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "productListTable",
        cols : [[
        	{field: 'id', title: 'ID', style:'height:100px;', minWidth:50, align:"center"},
            {field: 'name', title: '产品名称', style:'height:100px;', minWidth:50, align:"center"},
            {field: 'introduce', title: '产品介绍', style:'height:100px;', minWidth:150, align:"center"},
            {field: 'postage', title: '产品资费', style:'height:100px;', minWidth:50, align:"center"},
            {field: 'picture', title: '产品图片', style:'height:100px;', width: 150,templet:'<div><img src="{{ d.picture }}"></div>'},
            {field: 'prescription', title: '开通时效', style:'height:100px;', minWidth:50, align:"center",templet:function(d){
                if(d.prescription == "0"){
                    return "立即开通";
                }else if(d.prescription == "1"){
                    return "下月开通";
                }else if(d.prescription == "2"){
                	return "下季度开通";
                }else{
                    return "未知类型";
                }
            }},
            {field: 'createTime', title: '创建时间', style:'height:100px;', minWidth:50, align:"center"},
            {title: '操作', style:'height:100px;', minWidth:175, templet:'#productListBar',fixed:"right",align:"center"}
        ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if($(".searchVal").val() != ''){
        	setTimeout(function(){
        		table.reload("productListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	name: $(".searchVal").val()  //搜索的关键字
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });
    
  //选取产品
    function chooseProduct(choose){
    	flag=$("#flag").val();
    	parent.$(flag).val(choose.name);
    	if(flag=='#product1'){
    		parent.$('#primaryP').val(choose.id)	
    	}
    	if(flag=='#product2'){
    		parent.$('#hotP').val(choose.id)	
    	}
    }
    
    //列表操作
    table.on('tool(productList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'choose'){ //编辑
        	chooseProduct(data);
        }
    });
})
