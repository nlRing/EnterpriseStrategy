layui.config({
    base : "../static/js/"
}).extend({
    "address" : "orderaddress"
})
layui.use(['form','layer','table','jquery','address'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        table = layui.table,
        $ = layui.jquery,
       address = layui.address;
    
    //获取省信息
    address.provinces();
    
  //企业列表
    var tableIns = table.render({
        elem: '#gridList',
        cellMinWidth : 95,
        page : false,
        height : "360",
        initSort: {
            field: 'companyName' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "gridListTable",
        cols : [[
            {field: 'companyName', title: '公司名称', minWidth:100, align:"center"},
            {field: 'industry', minWidth:100, title: '行业', align:'center'},
            {field: 'state', minWidth:50, title: '状态', align:'center'},
            {field: 'legal', minWidth:50, title: '法人', align:'center'},
            {field: 'registered', minWidth:50, title: '注册资本', align:'center'},
            {field: 'telephone', minWidth:50, title: '联系电话', align:'center'},
            {field: 'address', minWidth:100, title: '企业地址',  align:'center'},
            {field: 'scope', minWidth:100, title: '经营范围',  align:'center'},
            {title: '操作', minWidth:100, templet:'#gridListBar',fixed:"right",align:"center"}
        ]]
    });
    
    form.on("submit(addTask)",function(data){
    	var name = $("#name").val().trim();
    	var describe = $("#describe").val();
	 	var primaryP = $("#primaryP").val().trim();
	 	var hotP = $("#hotP").val().trim();
	 	var city = data.field.city;
    	var area = data.field.area;
	 	var enterpriseIds;
	 	var tableData = [];
	 	if(!table.cache.gridListTable){
	 		layer.alert('请选择企业！');
	 	}else{
	 	 tableData = table.cache.gridListTable;
    	tableData.forEach(item=>{
    		enterpriseIds = enterpriseIds?(enterpriseIds+';'+item.id):item.id;
	       })
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
        	$.ajax({
    			type : "POST",
    			url : "../task/addBatch",
    			data : {
    				"name" : name,
    				"describe" : describe,
    				"primaryP" : primaryP,
    				"hotP" : hotP,
    				"enterpriseIds" : enterpriseIds,
    				"city" : city,
    				"area" : area
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				top.layer.close(index);
    	            top.layer.msg("任务添加成功！");
    	            layer.closeAll("iframe");
    	            //刷新父页面
    	            parent.location.reload();
    			}
    		});
        },2000);
        return false;
	 	}
    });
    

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());
    
    //添加产品
    function addProduct(flag){
    	var index = layui.layer.open({
            title : "产品列表",
            type : 2,
            area: ['1000px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            content : "../task/product",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                body.find('#flag').val(flag)
                setTimeout(function(){
                    layui.layer.tips('点击此处返回任务添加页面', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
    }
    
    //添加企业
    function addEnterprise(){
    	var index = layui.layer.open({
            title : "企业列表",
            type : 2,
            area: ['1000px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            content : "../task/enterprise",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回任务添加页面', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
    }   
    
  //列表操作
    table.on('tool(gridList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
       if(layEvent === 'del'){ //删除
    	var olddata = table.cache.gridListTable;
    	olddata.splice(obj.tr.data('index'),1);
    	tableIns.reload({data:olddata});
        }
    });
    
    $(".resettable").focus(function(){
    	table.reload('gridListTable',{data:[]});
    })
    $("#product1").focus(function(){
    	addProduct("#product1");
    })
    $("#product2").focus(function(){
    	addProduct("#product2");
    })
    $(".addEnterprise_btn").click(function(){
    	if($("#product1").val() && $("#product2").val()){
    		addEnterprise();
    	}else{
    		layer.alert("请先选择主推产品及热推产品！");
    	}
    	
    })
})