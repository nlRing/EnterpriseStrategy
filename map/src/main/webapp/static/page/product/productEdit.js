layui.use(['form','layer','upload','jquery'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        upload = layui.upload,
        $ = layui.jquery;
    
    //上传图片
    upload.render({
        elem: '.productFaceBtn',
        url: '../product/photoUpload',
        method : "post", 
        accept : "images",
        done: function(res, index, upload){
        	//console.log(res.data.src);
            $('#productFace').attr('src',"/file/"+res.data.src);
            console.log("/file/"+res.data.src);
            window.sessionStorage.setItem('productFace',"/file/"+res.data.src);
        }
    });
    
    form.on("submit(editProduct)",function(data){
    	var id = $("#id").val().trim();
    	var name = $("#name").val().trim();
    	var introduce = $("#introduce").val().trim();
	 	var postage = $("#postage").val().trim();
	 	var productSrc = $("#productFace").attr('src');
	 	var prescription = data.field.prescription;
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
        	$.ajax({
    			type : "POST",
    			url : "../product/edit",
    			data : {
    				"id" : id,
    				"name" : name,
    				"postage" : postage,
    				"picture" : productSrc,
    				"prescription" : prescription,
    				"introduce" : introduce
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				$("#productFace").attr('src',productSrc);//图片
    				//console.log($("#productFace").attr('src'));
    				//console.log(window.sessionStorage.getItem('productFace'));
                    window.sessionStorage.setItem('productFace',productSrc);
    				top.layer.close(index);
    	            top.layer.msg("产品修改成功！");
    	            layer.closeAll("iframe");
    	            //刷新父页面
    	            parent.location.reload();
    			}
    		});
        },2000);
        return false;
    });
    
    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})