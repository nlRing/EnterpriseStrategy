layui.use(['form','layer','jquery','upload'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        upload = layui.upload,
        $ = layui.jquery;
    
    //上传logo
    upload.render({
        elem: '#linkLogoImg',
        url: '../map/logoUpload',
        method : "post", 
        accept : "images",
        done: function(res, index, upload){
            $('#linkLogoImg').attr('src',"/file/"+res.data.src);
            //console.log($('#linkLogoImg').attr('src'));
            $('.linkLogo').css("background","#fff");
        }
    });
    
    form.on("submit(editGrid)",function(data){
    	var id = $("#id").val().trim();
    	var entType = $("#entType").val().trim();
    	//线下爬取
    	if(entType == '1'){
    		var province = $("#province").val().trim();
    		var cityName = $("#cityName").val().trim();
    		var companyName = $("#companyName").val().trim();
    		var state = $("#state").val().trim();
    		var legal = $("#legal").val().trim();
    		var registered = $("#registered").val().trim();
    		var telephone = $("#telephone").val().trim();
    		var address = $("#address").val().trim();
    		var website = $("#website").val().trim();
    		var email = $("#email").val().trim();
    		var develop = data.field.develop;
    		var industry = data.field.industry;
    		var type = data.field.type;
    		var scale = data.field.scale;
    		var scope = $("#scope").val().trim();
    		var picture = $(".linkLogoImg").attr('src');
    		var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
    		setTimeout(function(){
    			$.ajax({
    				type : "POST",
    				url : "../map/edit",
    				data : {
    					"id" : id,
    					"province" : province,
    					"cityName" : cityName,
    					"companyName" : companyName,
    					"state" : state,
    					"legal" : legal,
    					"registered" : registered,
    					"telephone" : telephone,
    					"address" : address,
    					"website" : website,
    					"email" : email,
    					"develop" : develop,
    					"industry" : industry,
    					"type" : type,
    					"scale" : scale,
    					"scope" : scope,
    					"picture" : picture,
    					"entType" : entType
    				},
    				dataType : "json",
    				success : function(jsonObject) {
    					top.layer.close(index);
    					top.layer.msg("修改企业成功！");
    					layer.closeAll("iframe");
    					//刷新父页面
    					parent.location.reload();
    				}
    			});
    		},2000);
    	//线上导入
    	}else if(entType == '0'){
    		var city = $("#cityName").val().trim();
    		var name = $("#companyName").val().trim();
    		var address = $("#address").val().trim();
    		var develop = data.field.develop;
    		var industry = data.field.industry;
    		var type = data.field.type;
    		var scale = data.field.scale;
    		var picture = $(".linkLogoImg").attr('src');
    		var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
    		setTimeout(function(){
    			$.ajax({
    				type : "POST",
    				url : "../map/editOnline",
    				data : {
    					"id" : id,
    					"city" : city,
    					"name" : name,
    					"companyName" : companyName,
    					"address" : address,
    					"develop" : develop,
    					"industry" : industry,
    					"type" : type,
    					"scale" : scale,
    					"picture" : picture,
    					"entType" : entType
    				},
    				dataType : "json",
    				success : function(jsonObject) {
    					top.layer.close(index);
    					top.layer.msg("修改企业成功！");
    					layer.closeAll("iframe");
    					//刷新父页面
    					parent.location.reload();
    				}
    			});
    		},2000);
    	}
        return false;
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})