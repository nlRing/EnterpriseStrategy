layui.config({
	base : "../static/js/"
}).extend({
    "address" : "orgedit"
})
layui.use(['form','layer','jquery','address'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;
    var address = layui.address;
    
    address.provinces(tempprovince,tempcity,temparea);
    
    form.on("submit(editChannel)",function(data){
    	var id = $("#id").val().trim();
    	var name = $("#name").val().trim();
    	var province = data.field.province;
    	var city = data.field.city;
    	var area = data.field.area;
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
        	$.ajax({
    			type : "POST",
    			url : "../organization/edit",
    			data : {
    				"id" : id,
    				"name" : name,
    				"province" : province,
    				"city" : city,
    				"area" : area
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				top.layer.close(index);
    	            top.layer.msg("渠道信息修改成功！");
    	            layer.closeAll("iframe");
    	            //刷新父页面
    	            parent.location.reload();
    			}
    		});
        },2000);
        return false;
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})