layui.use(['form','layer','laydate','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    //添加验证规则
    form.verify({
//        oldPwd : function(value, item){
//        	var val = $("#passwordInput").val(); //获取
//            if(value != val){
//                return "密码错误，请重新输入！";
//            }
//        },
        newPwd : function(value, item){
            if(value.length < 6){
                return "密码长度不能小于6位";
            }
        },
        confirmPwd : function(value, item){
            if(!new RegExp($("#pwd").val()).test(value)){
                return "两次输入密码不一致，请重新输入！";
            }
        }
    })

    form.on('switch(gradeStatus)', function(data){
        var tipText = '确定禁用当前会员等级？';
        if(data.elem.checked){
            tipText = '确定启用当前会员等级？'
        }
        layer.confirm(tipText,{
            icon: 3,
            title:'系统提示',
            cancel : function(index){
                data.elem.checked = !data.elem.checked;
                form.render();
                layer.close(index);
            }
        },function(index){
            layer.close(index);
        },function(index){
            data.elem.checked = !data.elem.checked;
            form.render();
            layer.close(index);
        });
    });
    
    //修改密码
    form.on("submit(changePwd)",function(data){
    	var index = layer.msg('提交中，请稍候',{icon: 16,time:false,shade:0.8});
    	var oldPwd=$("#oldPwd").val().trim();
		var password=$("#pwd").val().trim();
        setTimeout(function(){
            layer.close(index);
            var info = layer.msg("密码修改成功！请重新登录");
        	$.ajax({
    			type : "POST",
    			url : "../user/editUserPwd",
    			data : {
    				"oldPwd" : oldPwd,
    				"pwd" : password
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				setTimeout(function(){
    					if(!jsonObject.success){
    						layer.msg(jsonObject.msg, {icon: 5});
    					}else{
    						layer.close(info);
    						$.ajax({
    							type : "POST",
    							url : basePath+"/logout",
    							dataType : "json",
    							success : function(jsonObject) {
    								if (jsonObject.success) {
    									window.parent.location.href = basePath + '/';
    								}
    							},
    							error : function(data) {
    				                alert(data);
    				            }
    						});
    						//window.parent.location.href='../logout';
    					}
    				},2000);
    				//$("#pswForm")[0].submit();
    				//window.parent.location.reload(); //刷新父页面
    			}
    		});
        },2000);
    	return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });

    //控制表格编辑时文本的位置【跟随渲染时的位置】
    $("body").on("click",".layui-table-body.layui-table-main tbody tr td",function(){
        $(this).find(".layui-table-edit").addClass("layui-"+$(this).attr("align"));
    });

})