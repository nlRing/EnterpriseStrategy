layui.config({
	base: '../static/js/'
}).extend({
	formSelects: 'formSelects-v3'
})
layui.use(['form','layer','jquery','laytpl','formSelects'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        formSelects = layui.formSelects,
        $ = layui.jquery;
    
    $.get("../role/tree", function (data) {
    	data = JSON.parse(data);
    	var proHtml = '';
        for (var i = 0; i < data.length; i++) {
        	proHtml += '<option value="' + data[i].id + '">' + data[i].text + '</option>';
        }
        //初始化省数据
        $("select[name=roleIds]").append(proHtml);
        form.render();
    });
    
    $.get("../organization/treeGrid", function (data) {
    	data = JSON.parse(data);
    	var proHtml = '';
        for (var i = 0; i < data.length; i++) {
        	proHtml += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        }
        //初始化省数据
        $("select[name=organizationId]").append(proHtml);
        form.render();
    });
    
    //添加验证规则
    form.verify({
    	password : function(value, item){
            if(value.length < 6){
                return "密码长度不能小于6位";
            }
        }
    })
    
    form.on("submit(addUser)",function(data){
    	var organizationId = $("#organizationId").val();
    	var roleIds = formSelects.value('roleIds', 'valStr');;
    	var loginName = $("#loginName").val().trim();
	 	var name = $("#name").val().trim();
	 	var password = $("#password").val().trim();
	 	var age = $("#age").val().trim();
	 	var phone = $("#phone").val().trim();
    	var sex = data.field.sex;
    	var userType = data.field.userType;
    	//var roleIds = data.field.roleIds;
    	var status = data.field.status;
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
        	$.ajax({
    			type : "POST",
    			url : "../user/add",
    			data : {
    				"organizationId" : organizationId,
    				"loginName" : loginName,
    				"name" : name,
    				"password" : password,
    				"sex" : sex,
    				"userType" : userType,
    				"age" : age,
    				"roleIds" : roleIds,
    				"phone" : phone,
    				"status" : status
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				top.layer.close(index);
    	            top.layer.msg("用户添加成功！");
    	            layer.closeAll("iframe");
    	            //刷新父页面
    	            parent.location.reload();
    			}
    		});
        },2000);
        return false;
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})