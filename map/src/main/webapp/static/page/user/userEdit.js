layui.config({
	base: '../static/js/'
}).extend({
	formSelects: 'formSelects-v3'
})
layui.use(['form','layer','jquery','formSelects','laytpl'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        formSelects = layui.formSelects,
        laytpl = layui.laytpl,
        $ = layui.jquery;
    
    /**
     * 使用循环的方式判断一个元素是否存在于一个数组中
     * @param {Object} arr 数组
     * @param {Object} value 元素值
     */
    function isInArray(arr,value){
        for(var i = 0; i < arr.length; i++){
            if(value == arr[i]){
                return true;
            }
        }
        return false;
    }
    
    $.get("../role/tree", function (data) {
    	data = JSON.parse(data);
    	var proHtml = '';
        for (var i = 0; i < data.length; i++) {
        	if(isInArray(roleIds,data[i].id)){
        		proHtml += '<option value="' + data[i].id + '" selected=selected>' + data[i].text + '</option>';
        	}else{
        		proHtml += '<option value="' + data[i].id + '">' + data[i].text + '</option>';
        	}
        }
        //初始化省数据
        //$("select[id=roleIds]").append(proHtml);
        $("#roleIds").html(proHtml);
        formSelects.render('select');
    });
    
    $.get("../organization/treeGrid", function (data) {
    	data = JSON.parse(data);
    	var proHtml = '';
        for (var i = 0; i < data.length; i++) {
        	if(data[i].id == organizationId){
        		proHtml += '<option value="' + data[i].id + '" selected>' + data[i].name + '</option>';
        	}else{
        		proHtml += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        	}
        }
        //初始化省数据
        $("select[name=organizationId]").append(proHtml);
        form.render();
    });
    
    form.on("submit(editUser)",function(data){
    	var organizationId = $("#organizationId").val();
    	//var roleIds = $("#roleIds").combotree("getValues").toString();
    	var roleIds = formSelects.value('roleIds', 'valStr');
    	var loginName = $("#loginName").val().trim();
    	var id = $("#id").val().trim();
	 	var name = $("#name").val().trim();
	 	var password = $("#password").val().trim();
	 	var age = $("#age").val().trim();
	 	var phone = $("#phone").val().trim();
    	var sex = data.field.sex;
    	var userType = data.field.userType;
    	var userType = data.field.userType;
    	var status = data.field.status;
    	//var roleIds = data.field.roleIds;
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
        	$.ajax({
    			type : "POST",
    			url : "../user/edit",
    			data : {
    				"id" : id,
    				"loginName" : loginName,
    				"name" : name,
    				"password" : password,
    				"sex" : sex,
    				"userType" : userType,
    				"age" : age,
    				"roleIds" : roleIds,
    				"phone" : phone,
    				"status" : status,
    				"organizationId" : organizationId
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				top.layer.close(index);
    	            top.layer.msg("用户修改成功！");
    	            layer.closeAll("iframe");
    	            //刷新父页面
    	            parent.location.reload();
    			}
    		});
        },2000);
        return false;
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})