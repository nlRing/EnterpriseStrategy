layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //任务列表
    var tableIns = table.render({
        elem: '#taskList',
        method: 'post',
        url : '../order/taskPage?orderId='+parent.$('#orderId').val(),
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "taskListTable",
        cols : [[
        	 {field: 'id', title: 'id', minWidth:150, align:"center"},
        	 {field: 'province', title: '省份', minWidth:50, align:"center"},
             {field: 'cityName', title: '地市名称', minWidth:50, align:"center"},
             {field: 'companyName', title: '公司名称', minWidth:100, align:"center"},
             {field: 'industry', minWidth:100, title: '行业', align:'center'},
             {field: 'state', minWidth:50, title: '状态', align:'center'},
             {field: 'legal', minWidth:50, title: '法人', align:'center'},
             {field: 'registered', minWidth:50, title: '注册资本', align:'center'},
             {field: 'telephone', minWidth:50, title: '联系电话', align:'center'},
             {field: 'address', minWidth:100, title: '企业地址',  align:'center'},
             {field: 'website', minWidth:100, title: '企业网址',  align:'center'},
             {field: 'email', minWidth:80, title: '企业邮箱',  align:'center'},
             {field: 'develop', minWidth:80, title: '发展状态',  align:'center'},
             {field: 'scope', minWidth:100, title: '经营范围',  align:'center'},
            {field: 'primaryP', title: '主推产品', minWidth:100, align:"center"},
            {field: 'hotP', title: '热推产品', minWidth:100, align:"center"},
            {field: 'employeeId', title: '创建人工号', minWidth:100, align:"center"},
            {field: 'createTime', title: '创建时间', minWidth:100, align:"center"},
            {field: 'type', title: '类型', minWidth:100, align:"center",templet:function(d){
                if(d.type == "0"){
                    return "未分配";
                }else if(d.type == "1"){
                    return "已分配";
                }else if(d.type == "2"){
                	return "导入";
                }
            }}
            ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	console.log($(".searchVal").val());
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if($(".searchVal").val() != ''){
        	setTimeout(function(){
        		table.reload("taskListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	"companyName": $(".searchVal").val()  //搜索的关键字           
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });
    
    //列表操作
    table.on('tool(taskList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
       if(layEvent === 'choose'){ //删除
    	   chooseTask(data);
        }
    });
})
