layui.config({
    base : "../static/js/"
}).extend({
    "address" : "orderaddress"
})
layui.use(['form','layer','jquery','laytpl','upload','address'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        upload = layui.upload,
        address = layui.address,
        $ = layui.jquery;
    
    var fileName;
    var ids;
    var userIds;
    var tempHotproduct;
    
    //获取省信息
    address.provinces();
    
    //选完文件后不自动上传
    upload.render({
      elem: '#uploadBtn'
      ,url: '../order/upload'
      //,auto: false
      ,accept: 'file'
      ,exts: 'xls|xlsx'
      ,data: {
    	  city: function(){
    		    	return $('#city').val();
    		    },
    	  area: function(){
		    		return $('#area').val();
		    	}
    		}
      //,multiple: true
      //,bindAction: '#uploadFile'
      ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
    	  obj.preview(function(index, file, result){
    		  fileName = file.name;
    	      $("#file").val(fileName);
    	  });
      }
      ,done: function(res){
    	  if(JSON.stringify(res)=="{}"){
    		  layer.msg('上传文件格式错误,请检查文件格式重新上传！', {icon: 2});
    	  }
    	  ids = res.data.ids;
    	  //userIds = res.data.userIds
    	  layer.msg('上传成功', {icon: 6});
      }
      ,error: function(index, upload){
          //请求异常回调
    	  layer.msg('上传文件格式错误，员工工号不存在或者为选择地市、区县,请检查之后重新上传！', {icon: 2});
      }
    });
    
    //主推产品
    $.get("../product/treeGrid", function (data) {
    	data = JSON.parse(data);
    	var proHtml = '';
        for (var i = 0; i < data.length; i++) {
        	proHtml += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        }
        //主推产品
        $("#Pproduct").append(proHtml);
        form.render();
    });
    
    //热推产品
    $.get("../product/treeGrid", function (data) {
    	data = JSON.parse(data);
    	var proHtml = '';
        for (var i = 0; i < data.length; i++) {
        	proHtml += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        }
        //主推产品
        $("#Hotproduct").append(proHtml);
        //form.render();
        tempHotproduct = data[Math.floor(Math.random()*data.length)].id;
    });
    
//    form.on('select(Pproduct)', function(data){
//    	//console.log(data.elem); //得到select原始DOM对象
//    	//console.log(data.value); //得到被选中的值
//    	console.log(selectId);
//    	selectId = data.value;
//	});
    
    form.on("submit(addOrder)",function(data){
    	var name = $("#name").val().trim();
    	var describe = $("#describe").val();
    	var city = data.field.city;
    	var area = data.field.area;
	 	//fileName
    	var Pproduct = data.field.Pproduct;
    	//var Hotproduct = data.field.Hotproduct;
    	var Hotproduct = tempHotproduct;
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
        	$.ajax({
    			type : "POST",
    			url : "../order/add",
    			data : {
    				"ids" : ids,
    				//"userIds" : userIds,
    				"name" : name,
    				"describe" : describe,
    				"city" : city,
    				"country" : area,
    				"Pproduct" : Pproduct,
    				"Hotproduct" : Hotproduct
    			},
    			dataType : "json",
    			success : function(jsonObject) {
    				top.layer.close(index);
    	            top.layer.msg("工单添加成功！");
    	            layer.closeAll("iframe");
    	            //刷新父页面
    	            parent.location.reload();
    			}
    		});
        },2000);
        return false;
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})