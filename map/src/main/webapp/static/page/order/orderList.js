layui.config({
    base : "../static/js/"
}).extend({
    "address" : "orderaddress"
})
layui.use(['form','layer','table','laydate','laytpl','address'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        laydate = layui.laydate,
        table = layui.table,
        address = layui.address;
    
    //获取省信息
    address.provinces();
    
    var submitTime;
    //日期范围
    laydate.render({
      elem: '#release',
      range: true,
      done : function(value, date, endDate){
          submitTime = value.toString();
      }
    });
    
    //工单列表
    var tableOrder = table.render({
        elem: '#orderList',
        method: 'post',
        url : '../order/dataGrid',
        //cellMinWidth : 95,
        page : true,
        height : "full-140",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            //field: 'createTime' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "orderListTable",
        cols : [[
            {field: 'id', title: '序号', minWidth:50, align:"center"},
        	{field: 'name', title: '工单名称', minWidth:50, align:"center"},
            {field: 'city', title: '地市', minWidth:50, align:"center"},
            {field: 'country', title: '区县', minWidth:50, align:"center"},
            {field: 'createTime', title: '创建时间', minWidth:150, align:"center"},
            {field: 'cEmployeeId', minWidth:50, title: '创建人工号', align:'center'},
            {title: '操作', minWidth:175, templet:'#orderListBar',fixed:"right",align:"center"}
        ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
    	var name = $("#name").val().trim();
    	var employeeId = $("#userId").val();
    	var city = $("#city").find("option:selected").val();
    	var area = $("#area").find("option:selected").val();
    	var searchVal = ''+name+employeeId+city+area;
        if(searchVal != ''){
        	setTimeout(function(){
        		table.reload("orderListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	name: name,  //搜索的关键字
                    	employeeId: employeeId,
                    	city: city,
                    	country: area,
                    	submitTime: submitTime
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableOrder.reload();
        	layer.close(index);
        }
    });

    //添加成员
    function addOrder(){
        var index = layui.layer.open({
            title : "明细工单录入",
            type : 2,
            content : "../order/addOrder",
            end: function () {
                location.reload();
            },
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回工单列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
  //添加成员
    function createOrder(){
        var index = layui.layer.open({
            title : "创建工单",
            type : 2,
            content : "../task/addPage",
            end: function () {
                location.reload();
            },
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回工单列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    //成员企业列表
    function editUser(edit){
    	var index = layui.layer.open({
            title : "成员企业列表",
            type : 2,
            content : "../userep/manager?id="+edit.id,
            end: function () {
                location.reload();
            },
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回成员列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    
    $(".addOrder_btn").click(function(){
        addOrder();
    })
    
     $(".createOrder_btn").click(function(){
    	 createOrder();
    })

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('orderListTable'),
            data = checkStatus.data,
            newsId = [];
        if(data.length > 0) {
            for (var i in data) {
                newsId.push(data[i].newsId);
            }
            layer.confirm('确定删除选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
                // $.get("删除文章接口",{
                //     newsId : newsId  //将需要删除的newsId作为参数传入
                // },function(data){
                tableOrder.reload();
                layer.close(index);
                // })
            })
        }else{
            layer.msg("请选择需要删除的用户");
        }
    })
    
  //选择任务
    function taskDetails(data){
    	$('#orderId').val(data.id);
    	var index = layui.layer.open({
            title : "任务列表",
            type : 2,
            content : "../order/taskDetails",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回工单添加页面', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
         layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }   
    
  //添加任务
    function allocate(data){
    	$('#orderId').val(data.id);
        var index = layui.layer.open({
            title : "订单分配",
            type : 2,
            content : "../allocatedlist/addPage",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回任务列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }

    //列表操作
    table.on('tool(orderList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'details'){ //订单任务列表
        	taskDetails(data);
        }else if(layEvent === 'allocate'){ //订单任务列表
        	allocate(data);
        }else if(layEvent === 'del'){ //删除
        	var id = data.id;
            layer.confirm('确定删除此成员？',{icon:3, title:'提示信息'},function(index){
    			$.ajax({
      			type : "POST",
      			url : "../user/delMember",
      			data : {
      				"id" : id
      			},
      			success : function() {
      				top.layer.msg("成员删除成功！");
      	 			tableOrder.reload();
                    layer.close(index);
      			}
    			});
            });
        }
    });
})
