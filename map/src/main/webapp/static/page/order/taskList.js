layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //任务列表
    var tableIns = table.render({
        elem: '#taskList',
        method: 'post',
        url : '../order/taskPage?userId='+parent.$('#userId').val(),
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "taskListTable",
        cols : [[
        	 {field: 'id', title: 'id', minWidth:150, align:"center"},
            {field: 'companyName', title: '企业名称', minWidth:100, align:"center"},
            {field: 'province', title: '省份', minWidth:100, align:"center"},
            {field: 'city', title: '地市', minWidth:100, align:"center"},
            {field: 'primaryP', title: '主推产品', minWidth:100, align:"center"},
            {field: 'hotP', title: '热推产品', minWidth:100, align:"center"},
            {field: 'employeeId', title: '创建人工号', minWidth:100, align:"center"},
            {field: 'createTime', title: '创建时间', minWidth:100, align:"center"},
            {field: 'type', title: '类型', minWidth:100, align:"center",templet:function(d){
                if(d.type == "0"){
                    return "未分配";
                }else if(d.type == "1"){
                    return "已分配";
                }else if(d.type == "2"){
                	return "导入";
                }
            }},
            {title: '操作', minWidth:175, templet:'#taskListBar',fixed:"right",align:"center"}
        ]]
    });

    //搜索
    $(".search_btn").on("click",function(){
    	var companyName = $("#companyName").val()
    	var type = $("#type").val()
    	var searchVal = '' + companyName + type;
    	var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
        if(searchVal != ''){
        	setTimeout(function(){
        		table.reload("taskListTable",{
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    where: {
                    	"companyName": companyName,  //搜索的关键字
                    	"type": type
                    }
                });
    			layer.close(index);
    		},2000);
        }else{
            //layer.msg("请输入搜索的内容");
        	tableIns.reload();
        	layer.close(index);
        }
    });

    //选取企业
    function chooseTask(choose){
    	var fg = true;
    	var oldData = [];
    	var taskIds;
    	var len;
    	if(parent.table.cache.taskListTable){
    		oldData = parent.table.cache.taskListTable;
    	}
    	
        oldData.forEach(item=>{
            if(item.id==choose.id){
            	fg = false
                layer.alert('该任务已添加！');
            }
            len++;
        })
        if(fg){
        	if(len==10){
        		layer.alert("只能选择10个任务！");
        	}else{
        		oldData.push(choose);
                parent.table.reload('taskListTable',{data:oldData});
//                taskIds=parent.$('#taskIds').val()?parent.$('#taskIds').val()+';'+choose.id:choose.id	
//                parent.$('#taskIds').val(taskIds);		
        	}
        }
    }
    
    //列表操作
    table.on('tool(taskList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
       if(layEvent === 'choose'){ //删除
    	   chooseTask(data);
        }
    });
})
