layui.config({
    base : "../static/js/"
}).extend({
    "address" : "orderaddress"
})
layui.use(['form','layer','jquery','table','laytpl','upload','address'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        upload = layui.upload,
        address = layui.address,
        table = layui.table,
        $ = layui.jquery;   
    
    var fileName;
    var ids;
    var userIds;
    
    //获取省信息
    address.provinces();
    
    //任务列表
    var tableIns = table.render({
        elem: '#taskList',
        cellMinWidth : 95,
        page : false,
        height : "300",
        initSort: {
            field: 'id' //排序字段，对应 cols 设定的各字段名
            ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "taskListTable",
        cols : [[
        	 {field: 'id', title: 'id', minWidth:150, align:"center"},
            {field: 'companyName', title: '企业名称', minWidth:100, align:"center"},
            {field: 'province', title: '省份', minWidth:100, align:"center"},
            {field: 'city', title: '地市', minWidth:100, align:"center"},
            {field: 'primaryP', title: '主推产品', minWidth:100, align:"center"},
            {field: 'hotP', title: '热推产品', minWidth:100, align:"center"},
            {field: 'employeeId', title: '创建人工号', minWidth:100, align:"center"},
            {field: 'createTime', title: '创建时间', minWidth:100, align:"center"},
            {field: 'type', title: '类型', minWidth:100, align:"center",templet:function(d){
                if(d.type == "0"){
                    return "未分配";
                }else if(d.type == "1"){
                    return "已分配";
                }else if(d.type == "2"){
                	return "导入";
                }
            }},
            {title: '操作', minWidth:175, templet:'#taskListBar',fixed:"right",align:"center"}
        ]]
    });
    
    //选择人员
    function addMember(){
    	var index = layui.layer.open({
            title : "人员列表",
            type : 2,
            area: ['1000px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            content : "../order/member",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回工单添加页面', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
    }
    
    $("#employeeId").focus(function(){
    	addMember();
    })
    
    //选择任务
    function addTask(){
    	var index = layui.layer.open({
            title : "任务列表",
            type : 2,
            area: ['1000px', '500px'],
            fixed: false, //不固定
            maxmin: true,
            content : "../order/taskList",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                setTimeout(function(){
                    layui.layer.tips('点击此处返回工单添加页面', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
    }   
    
    $(".addTask_btn").click(function(){
    	if(!($('#employeeId').val())){
    		layer.alert('请先选择工号！');
    		$('#employeeId').focus();
    	}else{
    	addTask();
    }
    })
    
    form.on("submit(createOrder)",function(data){
    	var name = $("#name").val().trim();
    	var describe = $("#describe").val();
    	var userId = $("#userId").val().trim();
    	var taskIds;
    	if(!table.cache.taskListTable){
		 		layer.alert('请选择任务！');
		}else{
    	tableData = table.cache.taskListTable;
    	tableData.forEach(item=>{
    		taskIds = taskIds?taskIds+';'+item.id:item.id;
	       });
        //弹出loading
	        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
	        setTimeout(function(){
	        	$.ajax({
	    			type : "POST",
	    			url : "../order/addBatch",
	    			data : {
	    				"name" : name,
	    				"describe" : describe,
	    				"userId" : userId,
	    				"taskIds" : taskIds
	    			},
	    			dataType : "json",
	    			success : function(jsonObject) {
	    				top.layer.close(index);
	    	            top.layer.msg("工单添加成功！");
	    	            layer.closeAll("iframe");
	    	            //刷新父页面
	    	            parent.location.reload();
	    			}
	    		});
	        },2000);
	        return false;
		}
    });
    
    $(".resettable").focus(function(){
    	table.reload('taskListTable',{data:[]});
    })
    
    //列表操作
    table.on('tool(taskList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
       if(layEvent === 'del'){ //删除
    	   obj.del();
        }
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());
})