layui.use(['form','layer','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;
    
    //用户列表
    var tableIns = table.render({
        elem: '#syslogList',
        method: 'post',
        url : '../sysLog/dataGrid',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 20,
        initSort: {
            field: 'createTime' //排序字段，对应 cols 设定的各字段名
            ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        },
        id : "syslogTable",
        cols : [[
            {field: 'loginName', title: '登录名', minWidth:100, align:"center"},
            {field: 'roleName', title: '用户名', minWidth:100, align:"center"},
            {field: 'optContent', title: '内容', minWidth:150, align:"center"},
            {field: 'clientIp', minWidth:50, title: 'IP地址', align:'center'},
            {field: 'createTime', title: '登录时间', align:'center'}
        ]]
    });

})
