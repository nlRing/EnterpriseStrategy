//获取系统时间
var newDate = '';
getLangDate();
//值小于10时，在前面补0
function dateFilter(date){
    if(date < 10){return "0"+date;}
    return date;
}
function getLangDate(){
    var dateObj = new Date(); //表示当前系统时间的Date对象
    var year = dateObj.getFullYear(); //当前系统时间的完整年份值
    var month = dateObj.getMonth()+1; //当前系统时间的月份值
    var date = dateObj.getDate(); //当前系统时间的月份中的日
    var day = dateObj.getDay(); //当前系统时间中的星期值
    var weeks = ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
    var week = weeks[day]; //根据星期值，从数组中获取对应的星期字符串
    var hour = dateObj.getHours(); //当前系统时间的小时值
    var minute = dateObj.getMinutes(); //当前系统时间的分钟值
    var second = dateObj.getSeconds(); //当前系统时间的秒钟值
    var timeValue = "" +((hour >= 12) ? (hour >= 18) ? "晚上" : "下午" : "上午" ); //当前时间属于上午、晚上还是下午
    newDate = dateFilter(year)+"年"+dateFilter(month)+"月"+dateFilter(date)+"日 "+" "+dateFilter(hour)+":"+dateFilter(minute)+":"+dateFilter(second);
    document.getElementById("nowTime").innerHTML = "亲爱的"+userName+","+timeValue+"好！ 欢迎使用中小企业拓客平台 1.0。当前时间为： "+newDate+"　"+week;
    setTimeout("getLangDate()",1000);
}

layui.use(['form','element','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        element = layui.element;
        $ = layui.jquery;
    //上次登录时间【此处应该从接口获取，实际使用中请自行更换】
    $(".loginTime").html(newDate.split("日")[0]+"日</br>"+newDate.split("日")[1]);
    //icon动画
    $(".panel a").hover(function(){
        $(this).find(".layui-anim").addClass("layui-anim-scaleSpring");
    },function(){
        $(this).find(".layui-anim").removeClass("layui-anim-scaleSpring");
    })
    $(".panel a").click(function(){
        parent.addTab($(this));
    })
    //用户数量
    $.get("../user/userListCount",function(data){
        $(".userAll span").text(data);
    })
    //角色数量
    $.get("../role/roleListCount",function(data){
        $(".outIcons span").text(data);
    })
    
    var cityContainer = document.getElementById('cityChartId');
    //左边饼状图
	var cityChart = echarts.init(cityContainer);
	
	function loadCity(){
		cityChart.clear();
		cityChart.showLoading({text: '正在努力的读取数据中...'});
		$.ajax({
            type : "GET",
            async : false, //同步执行
            url : "../map/showEchartCity",
            dataType : "json", //返回数据形式为json
            success : function(data) {
            	var array = [];
            	for(var i = 0; i < data.length; i++) {
            		array.push(data[i].name);
            	}
                if (data) {
                	//指定图表的配置项和数据
                	option = {
                		title : {
                			text : '地市企业分布统计',
                			x : 'center'
                		},
                		tooltip : {
                			trigger : 'item',
                			formatter : "{a} <br/>{b} : {c} ({d}%)"
                		},
                		legend : {
                			orient : 'vertical',
                			left : 'left',
                			data : array
                		},
                		series : [ {
                			name : '企业数量',
                			type : 'pie',
                			radius : '55%',
                			center : [ '50%', '50%' ],
                			data : data,
                			itemStyle : {
                				emphasis : {
                					shadowBlur : 10,
                					shadowOffsetX : 0,
                					shadowColor : 'rgba(0, 0, 0, 0.5)'
                				},
                				normal : {
                                    label : {
                                        show : false   //隐藏标示文字
                                    },
                                    labelLine : {
                                        show : true   //隐藏标示线
                                    }
                                }
                			}
                		} ]
                	};
                    //使用刚指定的配置项和数据显示图表。
                	cityChart.setOption(option, true);
                	cityChart.hideLoading();
                }
            },
            error : function(errorMsg) {
                alert("图表请求数据失败啦!");
            }
        });
	}
	
	loadCity();
	
	var enterpriseContainer = document.getElementById('enterpriseChartId');
    //左边饼状图
	var enterpriseChart = echarts.init(enterpriseContainer);
	
	function loadEnterprise(){
		enterpriseChart.clear();
		enterpriseChart.showLoading({text: '正在努力的读取数据中...'});
		$.ajax({
            type : "GET",
            async : false, //同步执行
            url : "../map/showEchartEnterprise",
            dataType : "json", //返回数据形式为json
            success : function(data) {
            	var array = [];
            	for(var i = 0; i < data.length; i++) {
            		array.push(data[i].name);
            	}
                if (data) {
                	//指定图表的配置项和数据
                	option = {
                		title : {
                			text : '行业类型分布统计',
                			x : 'center'
                		},
                		tooltip : {
                			trigger : 'item',
                			formatter : "{a} <br/>{b} : {c} ({d}%)"
                		},
                		legend : {
                			orient : 'vertical',
                			type: 'scroll',
                			left : 'left',
                			data : array
                		},
                		series : [ {
                			name : '行业数量',
                			type : 'pie',
                			radius : '55%',
                			center : [ '50%', '50%' ],
                			data : data,
                			itemStyle : {
                				emphasis : {
                					shadowBlur : 10,
                					shadowOffsetX : 0,
                					shadowColor : 'rgba(0, 0, 0, 0.5)'
                				},
                                normal : {
                                    label : {
                                        show : false   //隐藏标示文字
                                    },
                                    labelLine : {
                                        show : true   //隐藏标示线
                                    }
                                }
                			}
                		} ]
                	};
                    //使用刚指定的配置项和数据显示图表。
                	enterpriseChart.setOption(option, true);
                	enterpriseChart.hideLoading();
                }
            },
            error : function(errorMsg) {
                alert("图表请求数据失败啦!");
            }
        });
	}
	
	loadEnterprise();
	
	var cityBusinessBarContainer = document.getElementById('cityBusinessBarId');
    //左下边柱状图
	var cityBusinessBar = echarts.init(cityBusinessBarContainer);
	
	function loadBusinessBar(){
		cityBusinessBar.clear();
		cityBusinessBar.showLoading({text: '正在努力的读取数据中...'});
		$.ajax({
            type : "GET",
            async : false, //同步执行
            url : "../map/showBarCity",
            dataType : "json", //返回数据形式为json
            success : function(data) {
            	var array = [];
            	var values = [];
            	for(var i = 0; i < data.length; i++) {
            		array.push(data[i].name);
            		values.push(data[i].value);
            	}
            	if(data){
            		option = {
            				tooltip: {
            					trigger: 'axis',
            					axisPointer: {
            						type: 'cross',
            						crossStyle: {
            							color: '#999'
            						}
            					}
            				},
            				legend: {
            					data:['地市']
            				},
            				xAxis: [
            				        {
            				        	type: 'category',
            				        	data: array,
            				        	axisPointer: {
            				        		type: 'shadow'
            				        	}
            				        }
            				        ],
            				        yAxis: [
            				                {
            				                	type: 'value',
            				                	name: '发展量',
            				                	//min: 0,
            				                	//max: 500000,
            				                	//interval: 10000,
            				                	axisLabel: {
            				                		formatter: '{value}'
            				                	}
            				                }
            				                ],
            				                series: [
            				                         {
            				                        	 name:'地市',
            				                        	 type:'bar',
            				                        	 data:values
            				                         }
            				                         ]
            		};
            	}
            }
		});
		//使用刚指定的配置项和数据显示图表。
		cityBusinessBar.setOption(option, true);
		cityBusinessBar.hideLoading();
	}
	
	loadBusinessBar();
	
	var enterpriseBarContainer = document.getElementById('enterpriseBarId');
    //右下边饼状图
	var enterpriseBar = echarts.init(enterpriseBarContainer);
	
	function loadEnterpriseBar(){
		enterpriseBar.clear();
		enterpriseBar.showLoading({text: '正在努力的读取数据中...'});
		$.ajax({
            type : "GET",
            async : false, //同步执行
            url : "../map/showBarEnterprise",
            dataType : "json", //返回数据形式为json
            success : function(data) {
            	var array = [];
            	var values = [];
            	for(var i = 0; i < data.length; i++) {
            		array.push(data[i].name);
            		values.push(data[i].value);
            	}
            	if(data){
            		option = {
            				tooltip: {
            					trigger: 'axis',
            					axisPointer: {
            						type: 'cross',
            						crossStyle: {
            							color: '#999'
            						}
            					}
            				},
            				legend: {
            					data:['行业']
            				},
            				xAxis: [
            				        {
            				        	type: 'category',
            				        	data: array,
            				        	axisPointer: {
            				        		type: 'shadow'
            				        	}
            				        }
            				        ],
            				        yAxis: [
            				                {
            				                	type: 'value',
            				                	name: '发展量',
            				                	axisLabel: {
            				                		formatter: '{value}'
            				                	}
            				                }
            				                ],
            				                series: [
            				                         {
            				                        	 name:'行业',
            				                        	 type:'bar',
            				                        	 data:values
            				                         }
            				                         ]
            		};
            	}
            }
		});
		//使用刚指定的配置项和数据显示图表。
		enterpriseBar.setOption(option, true);
		enterpriseBar.hideLoading();
	}
	
	loadEnterpriseBar();
})
