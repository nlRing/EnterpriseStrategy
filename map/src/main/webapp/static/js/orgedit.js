layui.define(["form","jquery"],function(exports){
    var form = layui.form,
    $ = layui.jquery,
    Address = {
        provinces : function(province,city,area) {
            //加载省数据
            var proHtml = '',that = this;
            $.get("../static/json/address.json", function (data) {
                for (var i = 0; i < data.length; i++) {
                	if(data[i].name == province){
                		proHtml += '<option value="' + data[i].name + '" selected>' + data[i].name + '</option>';
                	}else{
                		proHtml += '<option value="' + data[i].name + '">' + data[i].name + '</option>';
                	}
                }
                //城市数据初始化
                that.citys(data[7].childs,city);
                //城市数据初始化
                for (var i = 0; i < data.length; i++) {
                	var dataName = data[i].name;
                	if(dataName == province){
                		var cityNameArray = data[i].childs;
                		for (var i = 0; i < cityNameArray.length; i++) {
                			if(cityNameArray[i].name == city){
                				that.areas(cityNameArray[i].childs,area);
                			}
                		}
                	}
                }
                //初始化省数据
                $("select[name=province]").append(proHtml);
                form.render('select');
                //form.render();
                form.on('select(province)', function (proData) {
                    var value = proData.value;
                    console.log(value);
                    if (value != '' && value != null) {
                        that.citys(data[$(this).index() - 1].childs);
                    } else {
                        $("select[name=city]").attr("disabled", "disabled");
                    }
                });
            })
        },
        //加载市数据
        citys : function(citys,city) {
            var cityHtml = '<option value="">请选择市</option>',that = this;
            for (var i = 0; i < citys.length; i++) {
            	if(citys[i].name == city){
            		cityHtml += '<option value="' + citys[i].name + '" selected>' + citys[i].name + '</option>';
            	}else{
            		cityHtml += '<option value="' + citys[i].name + '">' + citys[i].name + '</option>';
            	}
            }
            $("select[name=city]").html(cityHtml).removeAttr("disabled");
            form.render('select');
            form.on('select(city)', function (cityData) {
                var value = cityData.value;
                //if (value > 0) {
                if (value != '' && value != null) {
                    that.areas(citys[$(this).index() - 1].childs);
                } else {
                    $("select[name=area]").attr("disabled", "disabled");
                }
            });
        },
        //加载县/区数据
        areas : function(areas,area) {
            var areaHtml = '<option value="">请选择县/区</option>';
            for (var i = 0; i < areas.length; i++) {
            	if(areas[i].name == area){
            		areaHtml += '<option value="' + areas[i].name + '"selected>' + areas[i].name + '</option>';
            	}else{
            		areaHtml += '<option value="' + areas[i].name + '">' + areas[i].name + '</option>';
            	}
                //areaHtml += '<option value="' + areas[i].code + '">' + areas[i].name + '</option>';
            }
            $("select[name=area]").html(areaHtml).removeAttr("disabled");
            form.render();
            form.on('select(area)', function (areaData) {
                var value = areaData.value;
            });
        }
    };
    exports("address",Address);
})