layui.define(["form","jquery",'table'],function(exports){
    var form = layui.form,
    table = layui.table,
    $ = layui.jquery,
    Address = {
        provinces : function() {
            //加载省数据
            var proHtml = '',that = this;
            $.get("../static/json/address.json", function (data) {
                for (var i = 0; i < data.length; i++) {
                	if(data[i].name == '黑龙江'){
                		proHtml += '<option value="' + data[i].name + '" selected>' + data[i].name + '</option>';
                	}else{
                		proHtml += '<option value="' + data[i].name + '">' + data[i].name + '</option>';
                	}
                }
                //城市数据初始化
                that.citys(data[7].childs);
                //初始化省数据
                $("select[name=province]").append(proHtml);
                form.render();
                form.on('select(province)', function (proData) {
                    $("select[name=area]").html('<option value="">请选择县/区</option>');
                    var value = proData.value;
                    //if (value > 0) {
                    if (value != '' && value != null) {
                        that.citys(data[$(this).index() - 1].childs);
                    } else {
                        $("select[name=city]").attr("disabled", "disabled");
                    }
                    
                    var province = $("#province").find("option:selected").val();
            		form.render('select'); //这个很重要
            		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
                	setTimeout(function(){
                		table.reload("gridListTable",{
                            page: {
                                curr: 1 //重新从第 1 页开始
                            },
                            where: {
                            	"province": province  //搜索的关键字
                            }
                        });
            			layer.close(index);
            		},2000);
                });
            })
        },
        //加载市数据
        citys : function(citys) {
            var cityHtml = '<option value="">请选择市</option>',that = this;
            for (var i = 0; i < citys.length; i++) {
                cityHtml += '<option value="' + citys[i].name + '">' + citys[i].name + '</option>';
                //cityHtml += '<option value="' + citys[i].code + '">' + citys[i].name + '</option>';
            }
            $("select[name=city]").html(cityHtml).removeAttr("disabled");
            form.render();
            form.on('select(city)', function (cityData) {
                var value = cityData.value;
                //if (value > 0) {
                if (value != '' && value != null) {
                    that.areas(citys[$(this).index() - 1].childs);
                } else {
                    $("select[name=area]").attr("disabled", "disabled");
                }
                
                var cityName = $("#city").find("option:selected").val();
        		form.render('select'); //这个很重要
        		var index = layer.msg('查询中，请稍候',{icon: 16,time:false,shade:0.8});
            	setTimeout(function(){
            		table.reload("gridListTable",{
                        page: {
                            curr: 1 //重新从第 1 页开始
                        },
                        where: {
                        	"cityName": cityName  //搜索的关键字
                        }
                    });
        			layer.close(index);
        		},2000);
            });
        },
        //加载县/区数据
        areas : function(areas) {
            var areaHtml = '<option value="">请选择县/区</option>';
            for (var i = 0; i < areas.length; i++) {
                areaHtml += '<option value="' + areas[i].name + '">' + areas[i].name + '</option>';
                //areaHtml += '<option value="' + areas[i].code + '">' + areas[i].name + '</option>';
            }
            $("select[name=area]").html(areaHtml).removeAttr("disabled");
            form.render();
        }
    };
    exports("address",Address);
})