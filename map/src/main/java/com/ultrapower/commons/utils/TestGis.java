package com.ultrapower.commons.utils;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import com.ultrapower.model.NetgridModel;
import com.ultrapower.model.OrderModel;

public class TestGis{

	 /**
     * 判断当前位置是否在围栏内
     */
	
	//dlist为某个多变形的所有点
	 public static boolean isInPolygon(OrderModel order,List<NetgridModel> dlist){
		 double p_x =Double.parseDouble(order.getLocationX());
		 double p_y =Double.parseDouble(order.getLocationY());
		 Point2D.Double point = new Point2D.Double(p_x, p_y);

		 List<Point2D.Double> pointList= new ArrayList<Point2D.Double>();
		 
		 for (NetgridModel enclosure : dlist){
			 //double polygonPoint_x=Double.parseDouble(enclosure.getLonX());
			 //double polygonPoint_y=Double.parseDouble(enclosure.getLatY());
			 double polygonPoint_x=enclosure.getLonX();
			 double polygonPoint_y=enclosure.getLatY();
			 Point2D.Double polygonPoint = new Point2D.Double(polygonPoint_x,polygonPoint_y);
			 pointList.add(polygonPoint);
		 }
		 TestGis test = new TestGis();
		 return test.checkWithJdkGeneralPath(point,pointList);
	 }
	 /**
	  * 返回一个点是否在一个多边形区域内
	  * @param point
	  * @param polygon
	  * @return
	  */
	private boolean checkWithJdkGeneralPath(Point2D.Double point, List<Point2D.Double> polygon) {
	       java.awt.geom.GeneralPath p = new java.awt.geom.GeneralPath();
		   Point2D.Double first = polygon.get(0);
		   p.moveTo(first.x, first.y);
		   polygon.remove(0);
		   for (Point2D.Double d : polygon) {
		      p.lineTo(d.x, d.y);
		   }
		   p.lineTo(first.x, first.y);
		   p.closePath();
		   return p.contains(point);
		}
	
	public static void main(String[] args) {
		OrderModel order=new OrderModel();
		order.setLocationX("106.607592");
		order.setLocationY("29.592114");
		List<NetgridModel> nlist = new ArrayList<NetgridModel>();
		NetgridModel ne =new NetgridModel();
		NetgridModel ne1 =new NetgridModel();
		NetgridModel ne2=new NetgridModel();
//		NetgridModel ne3=new NetgridModel();
//		ne.setLonX("106.493759");
//		ne.setLatY("29.581058");
		nlist.add(0, ne);
		
//		ne1.setLonX("106.501233");
//		ne1.setLatY("29.562964");
		nlist.add(1, ne1);
		
//		ne2.setLonX("106.539752");
//		ne2.setLatY("29.570504");
		nlist.add(2, ne2);
		
//		ne3.setLonX("4");
//		ne3.setLatY("2");
//		nlist.add(3, ne3);
		
		System.out.println(isInPolygon(order, nlist));
	}

}
