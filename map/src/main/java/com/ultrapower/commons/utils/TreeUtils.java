package com.ultrapower.commons.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ultrapower.commons.result.Menu;

public class TreeUtils {
  
	public static Map<Long,Menu> listToTree(List<Menu>  trees) {
		 Map<Long,Menu> treeMap = new HashMap<Long,Menu>();
         for(int i=0;i<trees.size();i++) {
        	 List<Menu> children = new ArrayList<Menu>();
        	 trees.get(i).setChildren(children);
        	 treeMap.put(trees.get(i).getId(), trees.get(i));
         }
         for(int i=0;i<trees.size();i++) {
        	 if(treeMap.containsKey(trees.get(i).getPid())) {
        		 treeMap.get(trees.get(i).getPid()).getChildren().add(trees.get(i));
        	 }
         }
        return treeMap;
	}
	
}
