package com.ultrapower.commons.result;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Menu implements java.io.Serializable{
	
	    private static final long serialVersionUID = 1L;
	    private Long id;
		private String title;
	    private String icon;
	    private String href;
	    private boolean spread;
	    private Long pid;
	    @JsonInclude(Include.NON_NULL)
	    private List<Menu> children; // null不输出
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
		public String getHref() {
			return href;
		}
		public void setHref(String href) {
			this.href = href;
		}
		public boolean isSpread() {
			return spread;
		}
		public void setSpread(boolean spread) {
			this.spread = spread;
		}
		public List<Menu> getChildren() {
			return children;
		}
		public void setChildren(List<Menu> children) {
			this.children = children;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getPid() {
			return pid;
		}
		public void setPid(Long pid) {
			this.pid = pid;
		}

}
