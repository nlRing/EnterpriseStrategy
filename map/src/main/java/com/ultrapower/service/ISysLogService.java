package com.ultrapower.service;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.SysLog;

/**
 *
 * SysLog 表数据服务层接口
 *
 */
public interface ISysLogService extends IService<SysLog> {

    void selectDataGrid(PageInfo pageInfo);

}