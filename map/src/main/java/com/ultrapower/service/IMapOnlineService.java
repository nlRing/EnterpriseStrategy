package com.ultrapower.service;


import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.model.EnterpriseOnline;

/**
*
* Map 表数据服务层接口
*
*/
public interface IMapOnlineService extends IService<EnterpriseOnline> {

	EnterpriseOnline selectById(int id);
	void updateByVo(EnterpriseOnline enterpriseOnline);
}
