package com.ultrapower.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.commons.result.Tree;
import com.ultrapower.model.Organization;

/**
 *
 * Organization 表数据服务层接口
 *
 */
public interface IOrganizationService extends IService<Organization> {

    List<Tree> selectTree();

    List<Organization> selectTreeGrid();
    
    void selectDataGrid(PageInfo pageInfo);
    
    Organization selectOrgByUserId(Long userId);

}