package com.ultrapower.service;

import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.Task;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sz
 * @since 2018-07-31
 */
public interface ITaskService extends IService<Task> {
	
	void selectDataGrid(PageInfo pageInfo);
	void selectTaskEnterprise(PageInfo pageInfo);
	boolean isExistence(int enterpriseId,int primaryProduct);
}
