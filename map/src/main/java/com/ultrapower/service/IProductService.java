package com.ultrapower.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.Product;

/**
*
* Product 表数据服务层接口
*
*/
public interface IProductService extends IService<Product>{

	void selectDataGrid(PageInfo pageInfo);
	Product selectProById(int id);
	List<Product> selectTreeGrid();
}
