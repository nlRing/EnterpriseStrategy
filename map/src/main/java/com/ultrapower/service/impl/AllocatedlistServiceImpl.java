package com.ultrapower.service.impl;

import com.ultrapower.model.Allocatedlist;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.mapper.AllocatedlistMapper;
import com.ultrapower.mapper.TaskMapper;
import com.ultrapower.service.IAllocatedlistService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sz
 * @since 2018-10-25
 */
@Service
public class AllocatedlistServiceImpl extends ServiceImpl<AllocatedlistMapper, Allocatedlist> implements IAllocatedlistService {
	@Autowired
    private AllocatedlistMapper allocatedlistMapper;

	@Override
	public void selectOrderEnterprise(PageInfo pageInfo) {
		// TODO Auto-generated method stub
		 Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
	        page.setOrderByField(pageInfo.getSort());
	        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
	        List<Map<String, Object>> list = allocatedlistMapper.selectOrderEnterprise(page, pageInfo.getCondition());
	        pageInfo.setData(list);
	        pageInfo.setCount(page.getTotal());
		
	}

	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		// TODO Auto-generated method stub
		Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
        page.setOrderByField(pageInfo.getSort());
        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
        List<Map<String, Object>> list = allocatedlistMapper.selectAllocatedPage(page, pageInfo.getCondition());
        pageInfo.setData(list);
        pageInfo.setCount(page.getTotal());	
	}
	
	@Override
	public void selectTaskPage(PageInfo pageInfo) {
		Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
        page.setOrderByField(pageInfo.getSort());
        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
        List<Map<String, Object>> list = allocatedlistMapper.selectTaskPage(page, pageInfo.getCondition());
        pageInfo.setData(list);
        pageInfo.setCount(page.getTotal());
	}

	@Override
	public int selectproductByAid(String allocateId) {
		// TODO Auto-generated method stub
		return allocatedlistMapper.selectproductByAid(allocateId);
	}
	
}
