package com.ultrapower.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.mapper.ProductMapper;
import com.ultrapower.model.Product;
import com.ultrapower.service.IProductService;

/**
*
* Product 表数据服务层接口实现类
*
*/
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService{
	
	@Autowired
    private ProductMapper productMapper;

	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
        page.setOrderByField(pageInfo.getSort());
        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
        List<Map<String, Object>> list = productMapper.selectProductPage(page, pageInfo.getCondition());
        pageInfo.setData(list);
        pageInfo.setCount(page.getTotal());
	}

	@Override
	public Product selectProById(int id) {
		return productMapper.selectProById(id);
	}

	@Override
	public List<Product> selectTreeGrid() {
		EntityWrapper<Product> wrapper = new EntityWrapper<Product>();
        wrapper.orderBy("id");
        return productMapper.selectList(wrapper);
	}

}
