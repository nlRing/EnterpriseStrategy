package com.ultrapower.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.mapper.TaskMapper;
import com.ultrapower.model.Task;
import com.ultrapower.service.ITaskService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sz
 * @since 2018-07-31
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {
	 @Autowired
	    private TaskMapper taskMapper;
	

	 @Override
	    public void selectDataGrid(PageInfo pageInfo) {
	        Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
	        page.setOrderByField(pageInfo.getSort());
	        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
	        List<Map<String, Object>> list = taskMapper.selectTaskPage(page, pageInfo.getCondition());
	        pageInfo.setData(list);
	        pageInfo.setCount(page.getTotal());
	    }
	 
	 @Override
	    public void selectTaskEnterprise(PageInfo pageInfo) {
	        Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
	        page.setOrderByField(pageInfo.getSort());
	        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
	        List<Map<String, Object>> list = taskMapper.selectTaskEnterprise(page, pageInfo.getCondition());
	        pageInfo.setData(list);
	        pageInfo.setCount(page.getTotal());
	    }

	@Override
	public boolean isExistence(int enterpriseId, int primaryProduct) {
		boolean flag = false;
		EntityWrapper<Task> wrapper = new EntityWrapper<Task>();
        wrapper.eq("enterprise_id", enterpriseId);
        wrapper.eq("primary_product", primaryProduct);
        List<Task> list = taskMapper.selectList(wrapper);
        if(list.size()>0){
        	flag = true;
        }
        return flag;
	}
	
}
