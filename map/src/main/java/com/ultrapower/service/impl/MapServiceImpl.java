package com.ultrapower.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.mapper.MapMapper;
import com.ultrapower.model.Enterprise;
import com.ultrapower.service.IMapService;

/**
*
* Enterprise 表数据服务层接口实现类
*
*/
@Service
public class MapServiceImpl extends ServiceImpl<MapMapper, Enterprise> implements IMapService{

	@Autowired
    private MapMapper mapMapper;
	
	@Override
	public List<Enterprise> selectByInd(Enterprise enterprise) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("province", enterprise.getProvince());
		params.put("cityName", enterprise.getCityName());
		params.put("industry", enterprise.getIndustry());
		params.put("develop", enterprise.getDevelop());
		params.put("type", enterprise.getType());
		params.put("scale", enterprise.getScale());
        return mapMapper.selectEnterpriseByPro(params);
	}
	
	@Override
	public List<Enterprise> selectForOutput(Enterprise enterprise) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cityName", enterprise.getCityName());
		params.put("industry", enterprise.getIndustry());
        return mapMapper.selectOutputEnterprise(params);
	}

	@Override
	public Enterprise selectById(int id) {
		return mapMapper.selectById(id);
	}

	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
        page.setOrderByField(pageInfo.getSort());
        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
        List<Map<String, Object>> list = mapMapper.selectEnterprisePage(page, pageInfo.getCondition());
        pageInfo.setData(list);
        pageInfo.setCount(page.getTotal());
	}

	@Override
	public List<Enterprise> selectcountDistance(Enterprise enterprise) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("province", enterprise.getProvince());
		params.put("cityName", enterprise.getCityName());
		params.put("industry", enterprise.getIndustry());
		params.put("longitude", enterprise.getLongitude());
		params.put("latitude", enterprise.getLatitude());
		params.put("radius", enterprise.getWebsite());
        return mapMapper.selectcountDistance(params);
	}

	@Override
	public List<Enterprise> selectByIds(String[] ids) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ids", ids);
		return mapMapper.selectExportData(params);
	}

	@Override
	public List<Enterprise> showEchartCity() {
		return mapMapper.selectCityChart();
	}

	@Override
	public List<Enterprise> showEchartEnterprise() {
		return mapMapper.showEchartEnterprise();
	}

	@Override
	public List<Enterprise> selectByName(Enterprise enterprise) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("companyName", enterprise.getCompanyName());
        return mapMapper.selectEnterpriseByName(params);
	}

	@Override
	public void updateByVo(Enterprise enterprise) {
		this.updateById(enterprise);
	}

	@Override
	public List<Enterprise> showBarCity() {
		return mapMapper.selectBarCity();
	}

	@Override
	public List<Enterprise> showBarEnterprise() {
		return mapMapper.showBarEnterprise();
	}

	@Override
	public List<Enterprise> selectcountPolygon(Enterprise enterprise) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("province", enterprise.getProvince());
		params.put("cityName", enterprise.getCityName());
		params.put("industry", enterprise.getIndustry());
		params.put("longitude", enterprise.getLongitude());
		params.put("latitude", enterprise.getLatitude());
        return mapMapper.selectcountPolygon(params);
	}

	@Override
	public List<String> selectAllName() {
		List<String> list = mapMapper.selectAllName();
		return list;
	}
}
