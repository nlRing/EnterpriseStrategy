package com.ultrapower.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.mapper.RoleResourceMapper;
import com.ultrapower.model.RoleResource;
import com.ultrapower.service.IRoleResourceService;

/**
 *
 * RoleResource 表数据服务层接口实现类
 *
 */
@Service
public class RoleResourceServiceImpl extends ServiceImpl<RoleResourceMapper, RoleResource> implements IRoleResourceService {


}