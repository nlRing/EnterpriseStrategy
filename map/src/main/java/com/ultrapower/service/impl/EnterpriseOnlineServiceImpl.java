package com.ultrapower.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.mapper.EnterpriseOnlineMapper;
import com.ultrapower.model.EnterpriseOnline;
import com.ultrapower.service.IEnterpriseOnlineService;

/**
*
* EnterpriseOnline 线上企业导入表
*
*/
@Service
public class EnterpriseOnlineServiceImpl extends ServiceImpl<EnterpriseOnlineMapper, EnterpriseOnline> implements IEnterpriseOnlineService{

}
