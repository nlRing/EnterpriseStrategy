package com.ultrapower.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.mapper.OrderMapper;
import com.ultrapower.model.Orderlist;
import com.ultrapower.service.IOrderService;
/**
 *
 * Organization 表数据服务层接口实现类
 *
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Orderlist> implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Orderlist> selectTreeGrid() {
        EntityWrapper<Orderlist> wrapper = new EntityWrapper<Orderlist>();
        wrapper.orderBy("seq");
        return orderMapper.selectList(wrapper);
    }

	@Override
	public void selectDataGrid(PageInfo pageInfo) {
		Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
        page.setOrderByField(pageInfo.getSort());
        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
        List<Map<String, Object>> list = orderMapper.selectOrderPage(page, pageInfo.getCondition());
        pageInfo.setData(list);
        pageInfo.setCount(page.getTotal());
	}
	
	@Override
	public void selectTaskPage(PageInfo pageInfo) {
		Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageInfo.getNowpage(), pageInfo.getSize());
        page.setOrderByField(pageInfo.getSort());
        page.setAsc(pageInfo.getOrder().equalsIgnoreCase("asc"));
        List<Map<String, Object>> list = orderMapper.selectTaskPage(page, pageInfo.getCondition());
        pageInfo.setData(list);
        pageInfo.setCount(page.getTotal());
	}
}