package com.ultrapower.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ultrapower.mapper.EnterpriseOnlineMapper;
import com.ultrapower.model.EnterpriseOnline;
import com.ultrapower.service.IMapOnlineService;

/**
*
* Enterprise 表数据服务层接口实现类
*
*/
@Service
public class MapOnlineServiceImpl extends ServiceImpl<EnterpriseOnlineMapper, EnterpriseOnline> implements IMapOnlineService{

	@Autowired
    private EnterpriseOnlineMapper enterpriseOnlineMapper;
	
	@Override
	public EnterpriseOnline selectById(int id) {
		return enterpriseOnlineMapper.selectById(id);
	}

	@Override
	public void updateByVo(EnterpriseOnline enterpriseOnline) {
		this.updateById(enterpriseOnline);
	}
}
