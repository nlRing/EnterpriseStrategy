package com.ultrapower.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.Tree;
import com.ultrapower.commons.shiro.ShiroUser;
import com.ultrapower.model.Resource;

/**
 *
 * Resource 表数据服务层接口
 *
 */
public interface IResourceService extends IService<Resource> {

    List<Resource> selectAll();

    List<Tree> selectAllMenu();

    List<Tree> selectAllTree();

    List<Tree> selectTree(ShiroUser shiroUser);
    
    String selectMenu(ShiroUser shiroUser);
}