package com.ultrapower.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.User;
import com.ultrapower.model.vo.UserVo;

/**
 *
 * User 表数据服务层接口
 *
 */
public interface IUserService extends IService<User> {

    List<User> selectByLoginName(UserVo userVo);

    void insertByVo(UserVo userVo);

    UserVo selectVoById(Long id);
    
    UserVo selectByEmployeeId(String employeeId);

    void updateByVo(UserVo userVo);

    void updatePwdByUserId(Long userId, String md5Hex);

    void selectDataGrid(PageInfo pageInfo);
    
    void selectAllMember(PageInfo pageInfo);
    
    void selectOutsider(PageInfo pageInfo);

    void deleteUserById(Long id);
    
    String selectMaxEId();
    
    int selectListCount();
    
    List<User> selectTreeGrid(User user);
}