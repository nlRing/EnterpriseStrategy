package com.ultrapower.service;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.model.EnterpriseOnline;

public interface IEnterpriseOnlineService extends IService<EnterpriseOnline> {

}
