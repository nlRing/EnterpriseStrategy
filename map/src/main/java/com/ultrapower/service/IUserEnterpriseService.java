package com.ultrapower.service;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.UserEnterprise;

/**
 *
 * UserRole 表数据服务层接口
 *
 */
public interface IUserEnterpriseService extends IService<UserEnterprise> {

    void selectEnterpriseByUser(PageInfo pageInfo);
	
	void selectOutsiderByUser(PageInfo pageInfo);
}