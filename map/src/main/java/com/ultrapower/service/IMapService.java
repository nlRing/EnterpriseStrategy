package com.ultrapower.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.Enterprise;

/**
*
* Map 表数据服务层接口
*
*/
public interface IMapService extends IService<Enterprise> {

	List<Enterprise> selectByInd(Enterprise enterprise);
	List<Enterprise> selectcountDistance(Enterprise enterprise);
	List<Enterprise> selectcountPolygon(Enterprise enterprise);
	Enterprise selectById(int id);
	void selectDataGrid(PageInfo pageInfo);
	List<Enterprise> selectForOutput(Enterprise enterprise);
	List<Enterprise> selectByIds(String[] ids);
	List<Enterprise> showEchartCity();
	List<Enterprise> showEchartEnterprise();
	List<Enterprise> showBarCity();
	List<Enterprise> showBarEnterprise();
	List<Enterprise> selectByName(Enterprise enterprise);
	void updateByVo(Enterprise enterprise);
	List<String> selectAllName();
}
