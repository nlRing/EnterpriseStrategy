package com.ultrapower.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.Orderlist;
/**
 *
 * Organization 表数据服务层接口
 *
 */
public interface IOrderService extends IService<Orderlist> {
	List<Orderlist> selectTreeGrid();
	void selectTaskPage(PageInfo pageInfo);
	void selectDataGrid(PageInfo pageInfo);
}