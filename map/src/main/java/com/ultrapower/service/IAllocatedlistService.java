package com.ultrapower.service;

import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.model.Allocatedlist;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sz
 * @since 2018-10-25
 */
public interface IAllocatedlistService extends IService<Allocatedlist> {
	void selectOrderEnterprise(PageInfo pageInfo);
	void selectDataGrid(PageInfo pageInfo);
	void selectTaskPage(PageInfo pageInfo);
	int selectproductByAid(String allocateId);
}
