package com.ultrapower.service;

import com.baomidou.mybatisplus.service.IService;
import com.ultrapower.model.RoleResource;

/**
 *
 * RoleResource 表数据服务层接口
 *
 */
public interface IRoleResourceService extends IService<RoleResource> {


}