package com.ultrapower.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author sz
 * @since 2018-08-07
 */
public class Task extends Model<Task> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	@TableField("enterprise_id")
	private Integer enterpriseId;
	@TableField("primary_product")
	private Integer primaryProduct;
	@TableField("hot_product")
	private Integer hotProduct;
	private Long creater;
	private Long userId;
	@TableField("order_id")
	private Integer orderId;
	@TableField("allocate_id")
	private Long allocateId;
	@TableField("create_time")
	private Date createTime;
	@TableField("update_time")
	private Date updateTime;
	@TableField("delete_flag")
	private Integer deleteFlag;
	@TableField("is_checked")
	private Integer isChecked;
	@TableField("type")
	private Integer type;
    

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(Integer enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public Integer getPrimaryProduct() {
		return primaryProduct;
	}

	public void setPrimaryProduct(Integer primaryProduct) {
		this.primaryProduct = primaryProduct;
	}

	public Integer getHotProduct() {
		return hotProduct;
	}

	public void setHotProduct(Integer hotProduct) {
		this.hotProduct = hotProduct;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Long getAllocateId() {
		return allocateId;
	}

	public void setAllocateId(Long allocateId) {
		this.allocateId = allocateId;
	}

	public Integer getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Integer isChecked) {
		this.isChecked = isChecked;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
