package com.ultrapower.model;

import java.io.Serializable;

import com.ultrapower.commons.utils.JsonUtils;

/**
*
* 企业
*
*/
public class Enterprise implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/** 主键id */
	private int id;
	/** 省份 */
	private String province;
	/** 地市 */
	private String cityName;
	/** 公司名称 */
	private String companyName;
	/** 行业 */
	private String industry;
	/** 状态 */
	private String state;
	/** 法人 */
	private String legal;
	/** 注册资本 */
	private String registered;
	/** 成立日期 */
	private String establishment;
	/** 联系电话 */
	private String telephone;
	/** 地址 */
	private String address;
	/** 企业网址 */
	private String website;
	/** 企业邮箱 */
	private String email;
	/** 经营范围 */
	private String scope;
	/** 经度 */
	private String longitude;
	/** 纬度 */
	private String latitude;
	/** 发展状态 */
	private String develop;
	/** 企业状态 */
	private String type;
	/** 企业规模 */
	private String scale;
	/** 图片地址 */
	private String picture;
	/** 企业分类（0：线上导入，1：线下爬取） */
	private int entType;
	
	public int getEntType() {
		return entType;
	}
	public void setEntType(int entType) {
		this.entType = entType;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getScale() {
		return scale;
	}
	public void setScale(String scale) {
		this.scale = scale;
	}
	public String getDevelop() {
		return develop;
	}
	public void setDevelop(String develop) {
		this.develop = develop;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLegal() {
		return legal;
	}
	public void setLegal(String legal) {
		this.legal = legal;
	}
	public String getRegistered() {
		return registered;
	}
	public void setRegistered(String registered) {
		this.registered = registered;
	}
	public String getEstablishment() {
		return establishment;
	}
	public void setEstablishment(String establishment) {
		this.establishment = establishment;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	@Override
	public String toString() {
		return JsonUtils.toJson(this);
	}
}
