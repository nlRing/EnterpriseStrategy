package com.ultrapower.model;

import java.io.Serializable;
import java.util.Date;

public class EnterpriseOnline implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/** 主键id */
	private int id;
	/** 公司名称 */
	private String name;
	/** 公司地址 */
	private String address;
	/** 地市 */
	private String city;
	/** 区县 */
	private String country;
	/** 创建时间 */
	private Date createTime;
	/** 经度 */
	private String longitude;
	/** 纬度 */
	private String latitude;
	/** 图片地址 */
	private String picture;
	/** 企业类型 */
	private String type;
	/** 企业规模 */
	private String scale;
	/** 发展状态 */
	private String develop;
	/** 企业分类（0：线上导入，1：线下爬取） */
	private int entType;
	/** 行业 */
	private String industry;
	
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getScale() {
		return scale;
	}
	public void setScale(String scale) {
		this.scale = scale;
	}
	public String getDevelop() {
		return develop;
	}
	public void setDevelop(String develop) {
		this.develop = develop;
	}
	public int getEntType() {
		return entType;
	}
	public void setEntType(int entType) {
		this.entType = entType;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
