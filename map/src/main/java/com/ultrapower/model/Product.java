package com.ultrapower.model;

import java.io.Serializable;
import java.sql.Timestamp;

import com.ultrapower.commons.utils.JsonUtils;

/**
*
* 产品
*
*/
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 主键id */
	private int id;
	/** 产品名称 */
	private String name;
	/** 图片路径 */
	private String picture;
	/** 产品介绍 */
	private String introduce;
	/** 产品资费 */
	private String postage;
	/** 开通时效 */
	private String prescription;
	/** 创建时间 */
	private Timestamp createTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getPostage() {
		return postage;
	}
	public void setPostage(String postage) {
		this.postage = postage;
	}
	public String getPrescription() {
		return prescription;
	}
	public void setPrescription(String prescription) {
		this.prescription = prescription;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	@Override
	public String toString() {
		return JsonUtils.toJson(this);
	}
}
