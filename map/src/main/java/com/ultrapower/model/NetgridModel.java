package com.ultrapower.model;

public class NetgridModel {

	private double lonX;
	private double latY;
	
	public double getLonX() {
		return lonX;
	}
	public void setLonX(double lonX) {
		this.lonX = lonX;
	}
	public double getLatY() {
		return latY;
	}
	public void setLatY(double latY) {
		this.latY = latY;
	}
	
}
