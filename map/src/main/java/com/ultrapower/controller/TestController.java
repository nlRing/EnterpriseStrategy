package com.ultrapower.controller;

import java.io.File;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ultrapower.commons.base.BaseController;
import com.ultrapower.model.Enterprise;
import com.ultrapower.service.IMapService;
import com.ultrapower.task.UploadEnterprise;

/**
 * @description：测试Controller
 * @author：zhixuan.wang
 * @date：2015/10/1 14:51
 */
@Controller
@RequestMapping("/test")
public class TestController extends BaseController {
	
	@Autowired
    private IMapService mapService;

    /**
     * 图标测试
     * 
     * @RequiresRoles shiro 权限注解
     * 
     * @return
     */
    @RequiresRoles("test")
    @GetMapping("/dataGrid")
    public String dataGrid() {
        return "admin/test";
    }

    /**
     * 下载测试
     * @return
     */
    @GetMapping("/down")
    public ResponseEntity<Resource> down(String cityName,String industry) {
    	if(cityName!=null && cityName.equals("请选择城市")) cityName="";
    	if(industry!=null && industry.equals("请选择行业")) industry="";
    	//查找出企业数据
    	Enterprise enterprise = new Enterprise();
    	enterprise.setCityName(cityName);
    	enterprise.setIndustry(industry);
    	List<Enterprise> enterprises = mapService.selectForOutput(enterprise);
    	System.out.println("导出企业的数量：" + enterprises.size());
    	//跟进查询出来的数据创建生产好Excel文件
    	File downloadFile = new File("D:\\download");
    	if(!downloadFile.exists()){
    		downloadFile.mkdir();
    	}
        File file = new UploadEnterprise().createDownloadExcel(downloadFile+"\\enterprise.xlsx", enterprises);
        System.out.println("##############cityName : "+cityName+ "      industry : " + industry + "          " + file.exists());
        return download(file);
    }
    
    /**
     * 商圈数据下载
     * @return
     */
    @GetMapping("/downData")
    public ResponseEntity<Resource> downData(String[] ids) {
    	File downloadFile = new File("D:\\download");
    	if(!downloadFile.exists()){
    		downloadFile.mkdir();
    	}
    	List<Enterprise> enterprises = mapService.selectByIds(ids);
    	System.out.println("导出企业的数量：" + enterprises.size());
        File file = new UploadEnterprise().createDownloadExcel(downloadFile+"\\data.xlsx", enterprises);
        return download(file);
    }
    
    /**
     * 模板下载
     * @return
     */
    @GetMapping("/downTmp")
    public ResponseEntity<Resource> downTmp() {
        File file = new File("D:/template/demo.xlsx");
        return download(file);
    }
}
