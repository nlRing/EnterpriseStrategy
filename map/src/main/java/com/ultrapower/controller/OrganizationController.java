package com.ultrapower.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ultrapower.commons.base.BaseController;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.commons.scan.Traced;
import com.ultrapower.commons.shiro.ShiroUser;
import com.ultrapower.commons.utils.StringUtils;
import com.ultrapower.model.Organization;
import com.ultrapower.model.User;
import com.ultrapower.service.IOrganizationService;
import com.ultrapower.service.IUserService;

/**
 * @description：部门管理
 * @author：zhixuan.wang
 * @date：2015/10/1 14:51
 */
@Controller
@RequestMapping("/organization")
public class OrganizationController extends BaseController {

    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IUserService userService;

    /**
     * 部门管理主页
     *
     * @return
     */
    @GetMapping(value = "/manager")
    public String manager() {
        return "admin/organization/organization";
    }
    
    /**
     * 渠道管理列表
     *
     * @param Channel
     * @param page
     * @param rows
     * @param sort
     * @param order
     * @return
     */
    @PostMapping("/dataGrid")
    @ResponseBody
    public Object dataGrid(Organization organization, Integer page, Integer limit, String sort, String order) {
    	sort = "createTime";
    	order = "asc";
    	ShiroUser shiroUser = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
    	User user = userService.selectById(shiroUser.getId());
    	Organization oi = organizationService.selectById(user.getOrganizationId());
        PageInfo pageInfo = new PageInfo(page, limit, sort, order);
        Map<String, Object> condition = new HashMap<String, Object>();
        if(StringUtils.isNotBlank(oi.getArea())){
        	condition.put("area", oi.getArea());
        }else if(StringUtils.isNotBlank(oi.getCity())){
        	condition.put("city", oi.getCity());
        }else if(StringUtils.isNotBlank(oi.getProvince())){
        	condition.put("province", oi.getProvince());
        }
        if (StringUtils.isNotBlank(organization.getName())) {
            condition.put("name", organization.getName());
        }
        pageInfo.setCondition(condition);
        organizationService.selectDataGrid(pageInfo);
        return pageInfo;
    }

    /**
     * 部门资源树
     *
     * @return
     */
    @PostMapping(value = "/tree")
    @ResponseBody
    public Object tree() {
        return organizationService.selectTree();
    }

    /**
     * 部门列表
     *
     * @return
     */
    @RequestMapping("/treeGrid")
    @ResponseBody
    public Object treeGrid() {
        return organizationService.selectTreeGrid();
    }

    /**
     * 添加部门页
     *
     * @return
     */
    @RequestMapping("/addPage")
    public String addPage() {
        return "admin/organization/organizationAdd";
    }

    /**
     * 添加部门
     *
     * @param organization
     * @return
     */
    @Traced(name="添加部门")
    @PostMapping("/add")
    @ResponseBody
    public Object add(Organization organization) {
        organization.setCreateTime(new Date());
        organizationService.insert(organization);
        return renderSuccess("添加成功！");
    }

    /**
     * 编辑部门页
     *
     * @param request
     * @param id
     * @return
     */
    @GetMapping("/editPage")
    public String editPage(Model model, Long id) {
        Organization organization = organizationService.selectById(id);
        model.addAttribute("organization", organization);
        return "admin/organization/organizationEdit";
    }

    /**
     * 编辑部门
     *
     * @param organization
     * @return
     */
    @Traced(name="编辑部门")
    @RequestMapping("/edit")
    @ResponseBody
    public Object edit(Organization organization) {
        organizationService.updateById(organization);
        return renderSuccess("编辑成功！");
    }

    /**
     * 删除部门
     *
     * @param id
     * @return
     */
    @Traced(name="删除部门")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
        organizationService.deleteById(id);
        return renderSuccess("删除成功！");
    }
}