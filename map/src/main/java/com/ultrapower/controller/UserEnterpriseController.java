package com.ultrapower.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ultrapower.commons.base.BaseController;
import com.ultrapower.commons.result.PageInfo;
import com.ultrapower.commons.utils.StringUtils;
import com.ultrapower.model.Enterprise;
import com.ultrapower.model.Role;
import com.ultrapower.model.UserEnterprise;
import com.ultrapower.service.IUserEnterpriseService;
import com.ultrapower.service.IUserService;

/**
 * @description：用户管理
 * @author：zhixuan.wang
 * @date：2015/10/1 14:51
 */
@Controller
@RequestMapping("/userep")
public class UserEnterpriseController extends BaseController {
    @Autowired
    private IUserService userService;
    @Autowired
    private IUserEnterpriseService userEnterpriseService;

    /**
     * 成员企业管理页
     *
     * @return
     */
    @GetMapping("/manager")
    public String manager(Model model, Long id) {
    	model.addAttribute("userid", id);
        return "admin/member/enterpriselist";
    }
    
    /**
     * 成员企业管理页
     *
     * @return
     */
    @GetMapping("/addep")
    public String addep(Model model, Long id) {
    	model.addAttribute("userid", id);
        return "admin/member/outsidereplist";
    }
    
    /**
     * 经理——成员管理
     *
     * @return
     */
    @GetMapping("/member")
    public String member() {
        return "admin/member/member";
    }

    /**
     * 已分配企业列表
     *
     * @param userVo
     * @param page
     * @param rows
     * @param sort
     * @param order
     * @return
     */
    @PostMapping("/Enterprise")
    @ResponseBody
    public Object Enterprise(Enterprise enterprise,String cityName,String industry,String companyName,String develop,Long id, Integer page, Integer limit, String sort, String order) {
    	sort = "id";
    	order = "asc";
        PageInfo pageInfo = new PageInfo(page, limit, sort, order);
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("userId", id);
        String province = enterprise.getProvince();
        if (StringUtils.isNotBlank(province)) {
        	condition.put("province", province);
        }
        if (StringUtils.isNotBlank(cityName)) {
            condition.put("cityName", cityName);
        }
        if (StringUtils.isNotBlank(industry)) {
            condition.put("industry", industry);
        }
        if (StringUtils.isNotBlank(develop) && !"0".equals(develop)) {
            condition.put("develop", develop);
        }
        if (StringUtils.isNotBlank(companyName)) {
            condition.put("companyName", companyName);
        }
        pageInfo.setCondition(condition);
        userEnterpriseService.selectEnterpriseByUser(pageInfo);
        return pageInfo;
    }
    
    /**
     * 已分配企业列表
     *
     * @param userVo
     * @param page
     * @param rows
     * @param sort
     * @param order
     * @return
     */
    @PostMapping("/outsiderEp")
    @ResponseBody
    public Object outsiderEp(Enterprise enterprise,String cityName,String industry,String companyName,String develop,Long id, Integer page, Integer limit, String sort, String order) {
    	sort = "id";
    	order = "asc";
        PageInfo pageInfo = new PageInfo(page, limit, sort, order);
        Map<String, Object> condition = new HashMap<String, Object>();
        condition.put("userId", id);
        String province = enterprise.getProvince();
        if (StringUtils.isNotBlank(province)) {
        	condition.put("province", province);
        }
        if (StringUtils.isNotBlank(cityName)) {
            condition.put("cityName", cityName);
        }else {  
        	pageInfo.setMsg("请选择省份和地市!");
        	return pageInfo;
        }
        if (StringUtils.isNotBlank(industry)) {
            condition.put("industry", industry);
        }
        if (StringUtils.isNotBlank(develop) && !"0".equals(develop)) {
            condition.put("develop", develop);
        }
        if (StringUtils.isNotBlank(companyName)) {
            condition.put("companyName", companyName);
        }
        pageInfo.setCondition(condition);
        userEnterpriseService.selectOutsiderByUser(pageInfo);
        return pageInfo;
    }
    
    /**
     * 为成员添加企业
     *
     * @param role
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public Object add(Long userId,Long enterpriseId) {
    	UserEnterprise userEnterprise = new UserEnterprise();
    	userEnterprise.setUserId(userId);
    	userEnterprise.setEnterpriseId(enterpriseId);
    	userEnterpriseService.insert(userEnterprise);
        return renderSuccess("添加成功！");
    }

    /**
     * 为成员删除企业
     *
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(Long id) {
    	userEnterpriseService.deleteById(id);
        return renderSuccess("删除成功！");
    }
}