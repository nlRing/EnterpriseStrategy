package com.ultrapower.mapper;

import com.ultrapower.model.Allocatedlist;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author sz
 * @since 2018-10-25
 */
public interface AllocatedlistMapper extends BaseMapper<Allocatedlist> {
	 List<Map<String, Object>> selectOrderEnterprise(Pagination page, Map<String, Object> params);
	 List<Map<String, Object>> selectAllocatedPage(Pagination page, Map<String, Object> params);
	 List<Map<String, Object>> selectTaskPage(Pagination page, Map<String, Object> params);
	 int selectproductByAid(String allocateId);
	 
}