package com.ultrapower.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.ultrapower.model.Organization;

/**
 *
 * Organization 表数据库控制层接口
 *
 */
public interface OrganizationMapper extends BaseMapper<Organization> {

	List<Map<String, Object>> selectOrgPage(Pagination page, Map<String, Object> params);
	Organization selectOrgByUserId(@Param(value="userId") Long userId);
}