package com.ultrapower.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.ultrapower.model.Orderlist;
/**
 *
 * Organization 表数据库控制层接口
 *
 */
public interface OrderMapper extends BaseMapper<Orderlist> {
	List<Map<String, Object>> selectOrderPage(Pagination page, Map<String, Object> params);
	List<Map<String, Object>> selectTaskPage(Pagination page, Map<String, Object> params);
}