package com.ultrapower.mapper;

import com.ultrapower.model.Task;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author sz
 * @since 2018-07-31
 */
public interface TaskMapper extends BaseMapper<Task> {
	
	List<Map<String, Object>> selectTaskPage(Pagination page, Map<String, Object> params);
    List<Map<String, Object>> selectTaskEnterprise(Pagination page, Map<String, Object> params);

}