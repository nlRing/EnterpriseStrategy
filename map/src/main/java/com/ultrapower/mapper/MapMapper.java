package com.ultrapower.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.ultrapower.model.Enterprise;

public interface MapMapper extends BaseMapper<Enterprise>{

	List<Enterprise> selectEnterpriseByPro(Map<String, Object> params);
	List<Enterprise> selectEnterpriseByName(Map<String, Object> params);
	List<Enterprise> selectcountDistance(Map<String, Object> params);
	List<Enterprise> selectcountPolygon(Map<String, Object> params);
	Enterprise selectById(@Param("id") int id);
	List<Map<String, Object>> selectEnterprisePage(Pagination page, Map<String, Object> params);
	
	List<Enterprise> selectOutputEnterprise(Map<String, Object> params);
	List<Enterprise> selectExportData(Map<String, Object> params);
	List<Enterprise> selectCityChart();
	List<Enterprise> showEchartEnterprise();
	void updateEnterprise(Map<String, Object> params);
	List<Enterprise> selectBarCity();
	List<Enterprise> showBarEnterprise();
	List<String> selectAllName();
}
