package com.ultrapower.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.ultrapower.model.Product;

public interface ProductMapper extends BaseMapper<Product> {

	List<Map<String, Object>> selectProductPage(Pagination page, Map<String, Object> params);
	Product selectProById(@Param("id") int id);
}
