package com.ultrapower.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.ultrapower.model.User;
import com.ultrapower.model.vo.UserVo;

/**
 *
 * User 表数据库控制层接口
 *
 */
public interface UserMapper extends BaseMapper<User> {

    UserVo selectUserVoById(@Param("id") Long id);
    
    UserVo selectUserByEmployeeId(@Param("employeeId") String employeeId);

    List<Map<String, Object>> selectUserPage(Pagination page, Map<String, Object> params);
    
    List<Map<String, Object>> selectUserPage(Map<String, Object> params);
    
    List<Map<String, Object>> selectOutsider(Pagination page, Map<String, Object> params);
    
    String selectMaxEId();
    
    int selectListCount();
}