package com.ultrapower.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.ultrapower.model.UserEnterprise;

/**
 *
 * UserEnterprise 表数据库控制层接口
 *
 */
public interface UserEnterpriseMapper extends BaseMapper<UserEnterprise> {

	List<Map<String, Object>> selectEnterpriseByUser(Pagination page, Map<String, Object> params);
	
	List<Map<String, Object>> selectOutsiderByUser(Pagination page, Map<String, Object> params);
    
    @Select("select role_id AS roleId from user_role where user_id = #{userId}")
    @ResultType(Long.class)
    List<Long> selectRoleIdListByUserId(@Param("userId") Long userId);

    @Delete("DELETE FROM user_role WHERE user_id = #{userId}")
    int deleteByUserId(@Param("userId") Long userId);

}