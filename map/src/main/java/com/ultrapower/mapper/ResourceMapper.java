package com.ultrapower.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ultrapower.model.Resource;

/**
 *
 * Resource 表数据库控制层接口
 *
 */
public interface ResourceMapper extends BaseMapper<Resource> {

}