package com.ultrapower.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ultrapower.model.EnterpriseOnline;

public interface EnterpriseOnlineMapper extends BaseMapper<EnterpriseOnline>{
	EnterpriseOnline selectById(@Param("id") int id);
}
