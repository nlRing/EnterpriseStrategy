package com.ultrapower.test.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TreeTest {
 
	public static void main(String args[]){
         Tree tree1 = new Tree();
         Tree tree12 = new Tree();
         Tree tree121 = new Tree();
         Tree tree2 = new Tree();
         Tree tree22 = new Tree();
         Tree tree221 = new Tree();
         tree1.setId(1);
         tree1.setName("系统管理");
         tree1.setPid(0);
         tree12.setId(12);
         tree12.setName("用户管理");
         tree12.setPid(1); 
         tree121.setId(121);
         tree121.setName("权限管理");
         tree121.setPid(12); 
         tree2.setId(2);
         tree2.setName("数据中心");
         tree2.setPid(0);
         tree22.setId(22);
         tree22.setName("地图展示");
         tree22.setPid(2);
         tree221.setId(221);
         tree221.setName("企业搜索");
         tree221.setPid(22);
         List<Tree> trees = new ArrayList<Tree>();
         trees.add(tree1);
         trees.add(tree12);
         trees.add(tree121);
         trees.add(tree2);
         trees.add(tree22);
         trees.add(tree221);
         Map<Long,Tree> treeMap = new HashedMap<Long,Tree>();
         for(int i=0;i<trees.size();i++) {
        	 List<Tree> children = new ArrayList<Tree>();
        	 trees.get(i).setChildren(children);
        	 treeMap.put(trees.get(i).getId(), trees.get(i));
         }
         for(int i=0;i<trees.size();i++) {
        	 if(treeMap.containsKey(trees.get(i).getPid())) {
        		 treeMap.get(trees.get(i).getPid()).getChildren().add(trees.get(i));
        	 }
         }
         ObjectMapper mapper = new ObjectMapper();
         String str = "";
         try {
			str = mapper.writeValueAsString(treeMap);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 System.out.println(str);
	 }

}
