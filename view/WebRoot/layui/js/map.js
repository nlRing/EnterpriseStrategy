/**
  项目JS主入口
  百度地图
**/
var map = new BMap.Map("allmap",{enableMapClick:false});            // 创建Map实例  
map.enableScrollWheelZoom();
map.centerAndZoom("哈尔滨",15);      // 初始化地图,用城市名设置地图中心点
map.setMapStyle({style:'light'});
var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT});// 右下角，添加比例尺
var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //右上角，仅包含平移和缩放按钮
/*缩放控件type有四种类型:
BMAP_NAVIGATION_CONTROL_SMALL：仅包含平移和缩放按钮；BMAP_NAVIGATION_CONTROL_PAN:仅包含平移按钮；BMAP_NAVIGATION_CONTROL_ZOOM：仅包含缩放按钮*/

map.addControl(top_left_control);        
map.addControl(top_left_navigation);     
map.addControl(top_right_navigation);

//map.addControl(new BMap.MapTypeControl());   //添加地图类型控件

var fbusinessopts = {
		width : 300,     // 信息窗口宽度
		height: 225,     // 信息窗口高度
		title : "渠道信息" , // 信息窗口标题
		enableMessage:true//设置允许信息窗发送短息
	   };

/**
  项目JS主入口
  以依赖Layui的layer和form模块为例
**/    
layui.config({
	base : "js/"
}).use(['form','element','layer','jquery'],function(){
	var form = layui.form,
	layer = layui.layer,
	element = layui.element;
	$ = layui.jquery;
	
	form.on('select(city)', function(data){
		var cityID = data.value;
		var cityName = $("#city").find("option:selected").text();
		$.ajax({
			type : "GET",
			url : "../cityController/getCountry",
			data : {
				"cityID" : cityID
			},
			dataType : "json",
			success : function(data) {
				var optionstring = "";
				$.each(data, function(i,item){
                    optionstring += "<option value=\"" + item.area + "\" >" + item.area + "</option>";
                });
                $("#country").html('<option value=""></option>' + optionstring);
				//$("#country").html(optionstring);
                form.render('select'); //这个很重要
                map.centerAndZoom(cityName,12);
			}
		});
	});
	
	form.on('select(country)', function(data){
		var countryName = data.value;
		map.centerAndZoom(countryName,15);
	});

	function showFbusinessinfo(e){
		var point = e.point;
		var content = point.content;
		var point = new BMap.Point(point.lng, point.lat);
		var infoWindow = new BMap.InfoWindow(content,fbusinessopts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口	
	}
	
	//查询
	$(".search_btn").click(function(){
		var local = new BMap.LocalSearch(map, {
			renderOptions:{map: map}
		});
		var text = $(".searchVal").val();
		if(text != ''){
			local.search(text);
		}
	});
	
	//显示现有渠道
	$(".showChannel_btn").click(function(){
		//layer.tips('只想提示的精准些', '.layui-layer-setwin .layui-layer-close');
		map.clearOverlays();
		var cityName = $("#city").find("option:selected").text();
		var countryName = $("#country").find("option:selected").text();
		var channelState = "正常运营";
		if(cityName != '请选择城市'){
			//获取现有渠道后台数据
			$.ajax({
				type : "POST",
				url : "../channelController/getChannelMapData",
				data : {
					"cityName" : cityName,
					"countryName" : countryName,
					"channelState" : channelState
				},
				dataType : "json",
				success : function(data) {
					layer.alert(JSON.stringify(data));
					var dataArr = [];
					var pointArr = [];
					for(var i=0;i<data.length;i++){
						var mPoint = new BMap.Point(data[i].longitude,data[i].latitude);
						pointArr.push(mPoint);
						dataArr.push(data[i]);
						//console.log(i+"-----------------");
						//这里10个一起转,因为官方接口最多只能批量转10个
						if(i%10==0){
							//84转百度
							translateLon_Lat(dataArr,pointArr);
							dataArr = [];
							pointArr = [];
						}
					}
					//这里是最后一批没有转换的
					if(dataArr.length>0){
						translateLon_Lat(dataArr,pointArr);
						dataArr = [];
						pointArr = [];
					}
				}
			});
		}else{
			layer.msg('请选择城市!');
		}
	});
	
	//清除地图
	$(".batchDel").click(function(){
		map.clearOverlays();
	});
	
	function addClickHandler(content,marker,opts,data){
		marker.addEventListener("click",function(e){
			openInfo(content,e,opts);
			}
		);
	}
	
	function openInfo(content,e,opts){
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content,opts);  // 创建信息窗口对象 
		map.openInfoWindow(infoWindow,point); //开启信息窗口
	}
});
