# 中小型企业战略平台

#### 项目介绍
运用大数据技术，借助互联网数据和运营商地理位置信息，通过科学有效的信息化手段实现中小微企业信息的系统采集，并通过准确合理的挖掘算法精准定位潜在客户群，利用GIS地图等数据可视化方法，构建全量政企市场战略地图，清晰反映省政企市场信息，从而指导各地市开展政企客户拓展工作，实现精细化营销，最终解决中小微企业信息收集困难、新客户发展艰难等问题

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)